import React from "react";
import Trusted from "../components/Trusted";
import Carousel from "../components/Carousel";
import Selling from "../components/Selling";
import Categories from "../components/Categories";
import FibBanner from "../components/FibBanner";
import Testimonials from "../components/Testimonials";
import LogoMakerBanner from "../components/LogoMakerBanner";
import MadeOnFiverr from "../components/MadeOnFiverr";
import FiverrGuides from "../components/FiverrGuides";
import Footer from "../components/Footer";
import NavBar from "../components/NavBar";
import Header from "../components/Header";

export default function HomePage() {
  return (
    <div
      style={{
        width: "100%",
        height: "100%",
        position: "relative",
        overflow: "hidden",
      }}
    >
      <NavBar />
      <Header />
      <Trusted />
      <Carousel />
      <Selling />
      <Categories />
      <FibBanner />
      <Testimonials />
      <LogoMakerBanner />
      <MadeOnFiverr />
      <FiverrGuides />
      <Footer />
    </div>
  );
}
