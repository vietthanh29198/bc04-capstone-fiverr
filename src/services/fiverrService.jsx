import { https } from "./configUrl";

export const fiverrServ = {
  layDanhSachCongViec: () => {
    let uri = "/api/cong-viec/lay-danh-sach-cong-viec-theo-ten/CongViec";
    return https.get(uri);
  },
};
