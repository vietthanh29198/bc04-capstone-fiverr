import React from "react";
import { makeStyles } from "@mui/styles";
import { Link } from "react-router-dom";

const useStyles = makeStyles({
  navBarWrapper: {
    borderColor: "transparent",
    width: "100%",
    transition: "border 0.5s ease",
  },
  navBar: {
    padding: "0 32px",
    flexWrap: "nowrap",
    alignContent: "center",
    height: "80px",
    justifyContent: "flex-start",
    alignItems: "center",
    display: "flex",
  },
  logo: {
    height: "auto",
    margin: "4px 0 0",
    minWidth: "121px",
    alignSelf: "auto",
    display: "block",
    width: "89px",
    textDecoration: "none",
    transition: "color 0.2s",
    color: "#1dbf73",
  },
  fiverrNav: {
    marginLeft: "32px",
    flexGrow: "1",
    justifyContent: "flex-end",
    display: "flex",
  },
  ul: {
    margin: "0",
    display: "flex",
    alignItems: "center",
    listStyle: "none",
    listStyleImage: "none",
    padding: "0",
  },
  a: {
    color: "#fff",
    fontWeight: "600",
    textDecoration: "none",
    whiteSpace: "nowrap",
    transition: "color 0.2s",
  },
  fiverrJoin: {
    borderColor: "#fff",
    color: "#fff",
    fontWeight: "700",
    padding: "7px 20px",
    transition: "all 0.2s ease",
    background: "none",
    fontSize: "14px",
    border: "1px solid transparent",
    borderRadius: "4px",
    boxSizing: "border-box",
    cursor: "pointer",
    display: "inline-block",
    lineHeight: "100%",
    position: "relative",
    textAlign: "center",
    textDecoration: "none",
    "&:hover": {
      background: "#1dbf73",
      borderColor: "#1dbf73",
      color: "#fff",
    },
  },
});

export default function NavBar() {
  const classes = useStyles();

  return (
    <div style={{ position: "fixed", width: "100%", zIndex: "9999" }}>
      <div className={classes.navBarWrapper}>
        <div className={classes.navBar}>
          <a href="/?source=top_nav" className={classes.logo}>
            <svg
              width={89}
              height={27}
              viewBox="0 0 89 27"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <g fill="#fff">
                <path d="m81.6 13.1h-3.1c-2 0-3.1 1.5-3.1 4.1v9.3h-6v-13.4h-2.5c-2 0-3.1 1.5-3.1 4.1v9.3h-6v-18.4h6v2.8c1-2.2 2.3-2.8 4.3-2.8h7.3v2.8c1-2.2 2.3-2.8 4.3-2.8h2zm-25.2 5.6h-12.4c.3 2.1 1.6 3.2 3.7 3.2 1.6 0 2.7-.7 3.1-1.8l5.3 1.5c-1.3 3.2-4.5 5.1-8.4 5.1-6.5 0-9.5-5.1-9.5-9.5 0-4.3 2.6-9.4 9.1-9.4 6.9 0 9.2 5.2 9.2 9.1 0 .9 0 1.4-.1 1.8zm-5.7-3.5c-.1-1.6-1.3-3-3.3-3-1.9 0-3 .8-3.4 3zm-22.9 11.3h5.2l6.6-18.3h-6l-3.2 10.7-3.2-10.8h-6zm-24.4 0h5.9v-13.4h5.7v13.4h5.9v-18.4h-11.6v-1.1c0-1.2.9-2 2.2-2h3.5v-5h-4.4c-4.3 0-7.2 2.7-7.2 6.6v1.5h-3.4v5h3.4z" />
              </g>
              <g fill="#1dbf73">
                <path d="m85.3 27c2 0 3.7-1.7 3.7-3.7s-1.7-3.7-3.7-3.7-3.7 1.7-3.7 3.7 1.7 3.7 3.7 3.7z" />
              </g>
            </svg>
          </a>

          <nav className={classes.fiverrNav}>
            <ul className={classes.ul}>
              <li
                className="display-from-md"
                style={{ padding: "0 10px", display: "block" }}
              >
                <a href="/start_selling?source=top_nav" className={classes.a}>
                  Become a Seller
                </a>
              </li>

              <Link to="/login" rel="nofollow" className={classes.a}>
                <li
                  className="display-from-sm"
                  style={{ padding: "0 10px", display: "block" }}
                >
                  Sign in
                </li>
              </Link>

              <Link to="/signin" className={classes.fiverrJoin} rel="nofollow">
                <li style={{ padding: "0 10px", display: "block" }}>Join</li>
              </Link>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  );
}
