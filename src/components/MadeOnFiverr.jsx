import React from "react";
import { makeStyles } from "@mui/styles";

const useStyles = makeStyles({
  sliderPackage: { border: "0", margin: "0", padding: "0", outline: "0" },
  slickSlider: {
    margin: "0 -18px",
    position: "relative",
    display: "block",
    boxSizing: "border-box",
    userSelect: "none",
    touchAction: "pan-y",
  },
  slickList: {
    display: "block",
    position: "relative",
    overflow: "hidden",
    margin: "0",
    padding: "0",
    transform: "translateZ(0)",
  },
  slickTrack: {
    display: "flex",
    justifyContent: "flex-start",
    position: "relative",
    left: "0px",
    transition: "left 500ms ease 0s",
    width: "7488px",
    opacity: "1",
    minWidth: "100%",
  },
  slickSlide: {
    width: "288px",
    padding: "0 18px 8px",
    float: "left",
    height: "100%",
    minHeight: "1px",
  },
  projectCard: {
    backgroundColor: "#fff",
    borderRadius: "4px",
    boxShadow:
      "0 0.14px 2.29266px rgb(0 0 0 / 3%), 0 0.37px 4.42626px rgb(0 0 0 / 5%), 0 3px 7px rgb(0 0 0 / 9%)",
  },
  imgProjectCard: {
    height: "237px",
    width: "100%",
    objectFit: "cover",
    borderRadius: "4px 4px 0 0",
    transition: "opacity .3s ease-in-out",
    display: "block",
    "&:hover": {
      opacity: "0.85",
    },
  },
  projectInfo: { display: "flex", padding: "16px 0 12px 16px" },
  imgProjectInfo: { height: "40px", width: "40px", borderRadius: "50%" },
  gigInfo: {
    fontSize: "14px",
    lineHeight: "21px",
    padding: "0 0 0 12px",
    width: "100%",
  },
  b: {
    display: "block",
    overflow: "hidden",
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
    width: "calc(100% - 12px)",
  },
  a1GigInfo: {
    fontSize: "14px",
    lineHeight: "21px",
    textDecoration: "none",
    color: "#404145",
    transition: "color .2s",
  },
  a2GigInfo: {
    fontSize: "14px",
    lineHeight: "21px",
    textDecoration: "none",
    color: "#95979d",
    transition: "color .2s",
  },
  a: {
    fontSize: "16px",
    lineHeight: "24px",
    textDecoration: "none",
    color: "#1dbf73",
    transition: "color .2s",
    "&:hover": { color: "#1dbf73" },
  },
  h2: {
    padding: "0 180px 24px 0",
    color: "#404145",
    fontSize: "32px",
    lineHeight: "120%",
    fontWeight: "700",
  },
  aSeeMoreLink: {
    fontSize: "16px",
    lineHeight: "24px",
    padding: "16px 0 0",
    top: "90px",
    right: "32px",
    textDecoration: "none",
    position: "absolute",
    color: "#446ee7",
    display: "block",
  },
  spanSeeMoreLink: {
    width: "10px",
    height: "10px",
    fill: "#446ee7",
    paddingLeft: "8px",
    background: "none",
    border: "none",
    display: "inline-block",
    margin: "0",
    padding: "0",
  },
  buttonPrev: {
    top: "50%",
    left: "-8px",
    cursor: "pointer",
    fontSize: "16px",
    lineHeight: "100%",
    textAlign: "center",
    position: "absolute",
    zIndex: "4",
    width: "48px",
    height: "48px",
    borderRadius: "30px",
    border: "0",
    boxShadow: "0 2px 5px 0 rgb(0 0 0 / 15%)",
    backgroundColor: "white",
    "&::after": {
      content: `''`,
      display: "inline-block",
      backgroundImage: `url("https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/arrows.a8b0bad.svg")`,
      backgroundRepeat: "no-repeat",
      backgroundSize: "16px 64px",
      width: "16px",
      height: "16px",
      marginTop: "3px",
    },
  },
  buttonNext: {
    top: "50%",
    right: "-8px",
    cursor: "pointer",
    fontSize: "16px",
    lineHeight: "100%",
    textAlign: "center",
    position: "absolute",
    zIndex: "4",
    width: "48px",
    height: "48px",
    borderRadius: "30px",
    border: "0",
    boxShadow: "0 2px 5px 0 rgb(0 0 0 / 15%)",
    backgroundColor: "white",
    "&::after": {
      content: `''`,
      display: "inline-block",
      backgroundImage: `url("https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/arrows.a8b0bad.svg")`,
      backgroundRepeat: "no-repeat",
      backgroundSize: "16px 64px",
      width: "16px",
      height: "16px",
      marginTop: "3px",
      backgroundPosition: "0 -48px",
    },
  },
});

export default function MadeOnFiverr() {
  const classes = useStyles();

  function clickNextOrPrev() {
    var listItems = document.getElementById("slick-track-made-on-fiverr");

    listItems.style.left =
      listItems.style.left == "0px" || listItems.style.left == "-1440px"
        ? listItems.style.left.substring(0, listItems.style.left.length - 2) -
          1440 +
          "px"
        : "0px";
  }

  return (
    <div
      className="made-on-fiverr-wrapper"
      style={{
        backgroundColor: "#f5f5f5",
        marginBottom: "96px",
      }}
    >
      <div
        className="made-on-fiverr"
        style={{ padding: "96px 32px", position: "relative" }}
      >
        <h2 className={classes.h2}>
          Get inspired with projects made by our freelancers
        </h2>

        <div className={classes.sliderPackage}>
          <div className={classes.slickSlider}>
            <button
              className={classes.buttonPrev}
              id="Previous-btn"
              onClick={clickNextOrPrev}
            ></button>

            <div className={classes.slickList}>
              <div
                className={classes.slickTrack}
                id="slick-track-made-on-fiverr"
              >
                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.projectCard}>
                      <a className={classes.a} href="">
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 2x"
                          />
                          <img
                            alt="Landing Page Design"
                            className={classes.imgProjectCard}
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 2x"
                            loading="lazy"
                          />
                        </picture>
                      </a>

                      <div className={classes.projectInfo}>
                        <a className={classes.a} href="">
                          <img
                            className={classes.imgProjectInfo}
                            alt="Seller profile image"
                            src="https://fiverr-res.cloudinary.com/t_profile_thumb,q_auto,f_auto/attachments/profile/photo/79cf5c7a560e6668555338b2831480e1-1539770224917/2bb8af3c-4cce-42a8-a699-f11177524084.png"
                            loading="lazy"
                          />
                        </a>
                        <span className={classes.gigInfo}>
                          <b className={classes.b}>
                            <a className={classes.a1GigInfo} href="">
                              Web &amp; Mobile Design
                            </a>
                          </b>
                          <a className={classes.a2GigInfo} href="">
                            by&nbsp;skydesigner
                          </a>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.projectCard}>
                      <a className={classes.a} href="">
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615096/passionshake.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615096/passionshake.jpeg 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615096/passionshake.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615096/passionshake.jpeg 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615096/passionshake.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615096/passionshake.jpeg 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615096/passionshake.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615096/passionshake.jpeg 2x"
                          />
                          <img
                            alt="Product Photography"
                            className={classes.imgProjectCard}
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615096/passionshake.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615096/passionshake.jpeg 2x"
                            loading="lazy"
                          />
                        </picture>
                      </a>

                      <div className={classes.projectInfo}>
                        <a className={classes.a} href="">
                          <img
                            className={classes.imgProjectInfo}
                            alt="Seller profile image"
                            src="https://fiverr-res.cloudinary.com/t_profile_thumb,q_auto,f_auto/attachments/profile/photo/ad68f554913d9d326d611e604ef79b0b-1608722734146/74e5ab33-a5fc-40ae-9cee-a91b23e80237.jpg"
                            loading="lazy"
                          />
                        </a>
                        <span className={classes.gigInfo}>
                          <b className={classes.b}>
                            <a className={classes.a1GigInfo} href="">
                              Product Photography
                            </a>
                          </b>
                          <a className={classes.a2GigInfo} href="">
                            by&nbsp;passionshake
                          </a>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.projectCard}>
                      <a className={classes.a} href="">
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/d51cf66f8a7026eb56a8c8e6b6793b24-1617027896306/lamonastudio-img.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/d51cf66f8a7026eb56a8c8e6b6793b24-1617027896306/lamonastudio-img.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_1.0/v1/attachments/generic_asset/asset/d51cf66f8a7026eb56a8c8e6b6793b24-1617027896306/lamonastudio-img.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_2.0/v1/attachments/generic_asset/asset/d51cf66f8a7026eb56a8c8e6b6793b24-1617027896306/lamonastudio-img.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_1.0/v1/attachments/generic_asset/asset/d51cf66f8a7026eb56a8c8e6b6793b24-1617027896306/lamonastudio-img.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_2.0/v1/attachments/generic_asset/asset/d51cf66f8a7026eb56a8c8e6b6793b24-1617027896306/lamonastudio-img.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/d51cf66f8a7026eb56a8c8e6b6793b24-1617027896306/lamonastudio-img.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/d51cf66f8a7026eb56a8c8e6b6793b24-1617027896306/lamonastudio-img.png 2x"
                          />
                          <img
                            alt="GIF Animation"
                            className={classes.imgProjectCard}
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/d51cf66f8a7026eb56a8c8e6b6793b24-1617027896306/lamonastudio-img.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/d51cf66f8a7026eb56a8c8e6b6793b24-1617027896306/lamonastudio-img.png 2x"
                            loading="lazy"
                          />
                        </picture>
                      </a>

                      <div className={classes.projectInfo}>
                        <a className={classes.a} href="">
                          <img
                            className={classes.imgProjectInfo}
                            alt="Seller profile image"
                            src="https://fiverr-res.cloudinary.com/t_profile_thumb,q_auto,f_auto/attachments/profile/photo/706891a4cc08826adca2819e14129aaf-1583755607494/5a706f4e-9f73-4ebc-97ff-9d2ef16bf28c.jpg"
                            loading="lazy"
                          />
                        </a>
                        <span className={classes.gigInfo}>
                          <b className={classes.b}>
                            <a className={classes.a1GigInfo} href="">
                              Animated GIFs
                            </a>
                          </b>
                          <a className={classes.a2GigInfo} href="">
                            by&nbsp;lamonastudio
                          </a>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.projectCard}>
                      <a className={classes.a} href="">
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/aa2ff6a65708e858cd563bedbc1f9e48-1617004762616/spickex.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/aa2ff6a65708e858cd563bedbc1f9e48-1617004762616/spickex.jpeg 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_1.0/v1/attachments/generic_asset/asset/aa2ff6a65708e858cd563bedbc1f9e48-1617004762616/spickex.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_2.0/v1/attachments/generic_asset/asset/aa2ff6a65708e858cd563bedbc1f9e48-1617004762616/spickex.jpeg 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_1.0/v1/attachments/generic_asset/asset/aa2ff6a65708e858cd563bedbc1f9e48-1617004762616/spickex.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_2.0/v1/attachments/generic_asset/asset/aa2ff6a65708e858cd563bedbc1f9e48-1617004762616/spickex.jpeg 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/aa2ff6a65708e858cd563bedbc1f9e48-1617004762616/spickex.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/aa2ff6a65708e858cd563bedbc1f9e48-1617004762616/spickex.jpeg 2x"
                          />
                          <img
                            alt="Catalog Design"
                            className={classes.imgProjectCard}
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/aa2ff6a65708e858cd563bedbc1f9e48-1617004762616/spickex.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/aa2ff6a65708e858cd563bedbc1f9e48-1617004762616/spickex.jpeg 2x"
                            loading="lazy"
                          />
                        </picture>
                      </a>

                      <div className={classes.projectInfo}>
                        <a className={classes.a} href="">
                          <img
                            className={classes.imgProjectInfo}
                            alt="Seller profile image"
                            src="https://fiverr-res.cloudinary.com/t_profile_thumb,q_auto,f_auto/profile/photos/22711136/original/fiverr_profile.jpg"
                            loading="lazy"
                          />
                        </a>
                        <span className={classes.gigInfo}>
                          <b className={classes.b}>
                            <a className={classes.a1GigInfo} href="">
                              Flyer Design
                            </a>
                          </b>
                          <a className={classes.a2GigInfo} href="">
                            by&nbsp;spickex
                          </a>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.projectCard}>
                      <a className={classes.a} href="">
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/annapietrangeli.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/annapietrangeli.jpeg 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/annapietrangeli.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/annapietrangeli.jpeg 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/annapietrangeli.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/annapietrangeli.jpeg 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/annapietrangeli.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/annapietrangeli.jpeg 2x"
                          />
                          <img
                            alt="Book Cover"
                            className={classes.imgProjectCard}
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/annapietrangeli.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/annapietrangeli.jpeg 2x"
                            loading="lazy"
                          />
                        </picture>
                      </a>

                      <div className={classes.projectInfo}>
                        <a className={classes.a} href="">
                          <img
                            className={classes.imgProjectInfo}
                            alt="Seller profile image"
                            src="https://fiverr-res.cloudinary.com/t_profile_thumb,q_auto,f_auto/attachments/profile/photo/8b60be1bf2915ddc1d551eaa252684d7-1589020928117/1d531e54-7607-4bdb-815f-088dbc0fb971.jpg"
                            loading="lazy"
                          />
                        </a>
                        <span className={classes.gigInfo}>
                          <b className={classes.b}>
                            <a className={classes.a1GigInfo} href="">
                              Book Design
                            </a>
                          </b>
                          <a className={classes.a2GigInfo} href="">
                            by&nbsp;annapietrangeli
                          </a>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.projectCard}>
                      <a className={classes.a} href="">
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615077/eveeelin.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615077/eveeelin.jpeg 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615077/eveeelin.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615077/eveeelin.jpeg 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615077/eveeelin.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615077/eveeelin.jpeg 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615077/eveeelin.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615077/eveeelin.jpeg 2x"
                          />
                          <img
                            alt="Brand Style Guides"
                            className={classes.imgProjectCard}
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615077/eveeelin.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615077/eveeelin.jpeg 2x"
                            loading="lazy"
                          />
                        </picture>
                      </a>

                      <div className={classes.projectInfo}>
                        <a className={classes.a} href="">
                          <img
                            className={classes.imgProjectInfo}
                            alt="Seller profile image"
                            src="https://fiverr-res.cloudinary.com/t_profile_thumb,q_auto,f_auto/attachments/profile/photo/27bdb82e27e444fe2b27aa7b3083cee8-1591694084918/f79ede47-da5f-440a-bf23-57ed9ef7d363.png"
                            loading="lazy"
                          />
                        </a>
                        <span className={classes.gigInfo}>
                          <b className={classes.b}>
                            <a className={classes.a1GigInfo} href="">
                              Logo Design
                            </a>
                          </b>
                          <a className={classes.a2GigInfo} href="">
                            by&nbsp;eveeelin
                          </a>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.projectCard}>
                      <a className={classes.a} href="">
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615068/mijalzagier.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615068/mijalzagier.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615068/mijalzagier.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615068/mijalzagier.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615068/mijalzagier.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615068/mijalzagier.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615068/mijalzagier.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615068/mijalzagier.png 2x"
                          />
                          <img
                            alt="Product Packaging"
                            className={classes.imgProjectCard}
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615068/mijalzagier.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615068/mijalzagier.png 2x"
                            loading="lazy"
                          />
                        </picture>
                      </a>

                      <div className={classes.projectInfo}>
                        <a className={classes.a} href="">
                          <img
                            className={classes.imgProjectInfo}
                            alt="Seller profile image"
                            src="https://fiverr-res.cloudinary.com/t_profile_thumb,q_auto,f_auto/attachments/profile/photo/58960b09956dc710d2d5a33573261936-1554984111113/750ccab0-8a64-4c91-b9a4-d10039dbf79c.jpg"
                            loading="lazy"
                          />
                        </a>
                        <span className={classes.gigInfo}>
                          <b className={classes.b}>
                            <a className={classes.a1GigInfo} href="">
                              Packaging Design
                            </a>
                          </b>
                          <a className={classes.a2GigInfo} href="">
                            by&nbsp;mijalzagier
                          </a>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.projectCard}>
                      <a className={classes.a} href="">
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615069/noneyn.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615069/noneyn.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615069/noneyn.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615069/noneyn.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615069/noneyn.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615069/noneyn.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615069/noneyn.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615069/noneyn.png 2x"
                          />
                          <img
                            alt="Realistic Portrait"
                            className={classes.imgProjectCard}
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615069/noneyn.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615069/noneyn.png 2x"
                            loading="lazy"
                          />
                        </picture>
                      </a>

                      <div className={classes.projectInfo}>
                        <a className={classes.a} href="">
                          <img
                            className={classes.imgProjectInfo}
                            alt="Seller profile image"
                            src="https://fiverr-res.cloudinary.com/t_profile_thumb,q_auto,f_auto/attachments/profile/photo/23b01eca3b78e2869e149efe15d3066a-1613424545655/0aaffa8e-01e0-4dcb-b56d-674e9b9c4bf5.jpg"
                            loading="lazy"
                          />
                        </a>
                        <span className={classes.gigInfo}>
                          <b className={classes.b}>
                            <a className={classes.a1GigInfo} href="">
                              Portraits &amp; Caricatures
                            </a>
                          </b>
                          <a className={classes.a2GigInfo} href="">
                            by&nbsp;noneyn
                          </a>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.projectCard}>
                      <a className={classes.a} href="">
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/bruno_malagrino.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/bruno_malagrino.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/bruno_malagrino.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/bruno_malagrino.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/bruno_malagrino.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/bruno_malagrino.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/bruno_malagrino.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/bruno_malagrino.png 2x"
                          />
                          <img
                            alt="Logo Design"
                            className={classes.imgProjectCard}
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/bruno_malagrino.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/bruno_malagrino.png 2x"
                            loading="lazy"
                          />
                        </picture>
                      </a>

                      <div className={classes.projectInfo}>
                        <a className={classes.a} href="">
                          <img
                            className={classes.imgProjectInfo}
                            alt="Seller profile image"
                            src="https://fiverr-res.cloudinary.com/t_profile_thumb,q_auto,f_auto/attachments/profile/photo/c15f6b22da97be41a8878e753a1a16c2-863645391592368980.489561/AF1BF970-07CA-454B-8AF1-2F3E06838C8B"
                            loading="lazy"
                          />
                        </a>
                        <span className={classes.gigInfo}>
                          <b className={classes.b}>
                            <a className={classes.a1GigInfo} href="">
                              Logo Design
                            </a>
                          </b>
                          <a className={classes.a2GigInfo} href="">
                            by&nbsp;bruno_malagrino
                          </a>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.projectCard}>
                      <a className={classes.a} href="">
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615082/christophbrandl.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615082/christophbrandl.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615082/christophbrandl.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615082/christophbrandl.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615082/christophbrandl.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615082/christophbrandl.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615082/christophbrandl.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615082/christophbrandl.png 2x"
                          />
                          <img
                            alt="Illustration"
                            className={classes.imgProjectCard}
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615082/christophbrandl.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615082/christophbrandl.png 2x"
                            loading="lazy"
                          />
                        </picture>
                      </a>

                      <div className={classes.projectInfo}>
                        <a className={classes.a} href="">
                          <img
                            className={classes.imgProjectInfo}
                            alt="Seller profile image"
                            src="https://fiverr-res.cloudinary.com/t_profile_thumb,q_auto,f_auto/attachments/profile/photo/b615b780b5c813d932953d05ec10f811-1596879215580/6b4a9867-ad06-415f-b307-11177ae30fdd.jpeg"
                            loading="lazy"
                          />
                        </a>
                        <span className={classes.gigInfo}>
                          <b className={classes.b}>
                            <a className={classes.a1GigInfo} href="">
                              Illustration
                            </a>
                          </b>
                          <a className={classes.a2GigInfo} href="">
                            by&nbsp;christophbrandl
                          </a>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.projectCard}>
                      <a className={classes.a} href="">
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615084/fernandobengua.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615084/fernandobengua.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615084/fernandobengua.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615084/fernandobengua.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615084/fernandobengua.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615084/fernandobengua.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615084/fernandobengua.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615084/fernandobengua.png 2x"
                          />
                          <img
                            alt="Social Media Design"
                            className={classes.imgProjectCard}
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615084/fernandobengua.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615084/fernandobengua.png 2x"
                            loading="lazy"
                          />
                        </picture>
                      </a>

                      <div className={classes.projectInfo}>
                        <a className={classes.a} href="">
                          <img
                            className={classes.imgProjectInfo}
                            alt="Seller profile image"
                            src="https://fiverr-res.cloudinary.com/t_profile_thumb,q_auto,f_auto/attachments/profile/photo/0738007a913d22569fe2b049f9259526-1589210299120/db111eb4-c119-42b4-9a1d-9116601f3d22.png"
                            loading="lazy"
                          />
                        </a>
                        <span className={classes.gigInfo}>
                          <b className={classes.b}>
                            <a className={classes.a1GigInfo} href="">
                              Social Media Design
                            </a>
                          </b>
                          <a className={classes.a2GigInfo} href="">
                            by&nbsp;fernandobengua
                          </a>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.projectCard}>
                      <a className={classes.a} href="">
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 2x"
                          />
                          <img
                            alt="Landing Page Design"
                            className={classes.imgProjectCard}
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 2x"
                            loading="lazy"
                          />
                        </picture>
                      </a>

                      <div className={classes.projectInfo}>
                        <a className={classes.a} href="">
                          <img
                            className={classes.imgProjectInfo}
                            alt="Seller profile image"
                            src="https://fiverr-res.cloudinary.com/t_profile_thumb,q_auto,f_auto/attachments/profile/photo/79cf5c7a560e6668555338b2831480e1-1539770224917/2bb8af3c-4cce-42a8-a699-f11177524084.png"
                            loading="lazy"
                          />
                        </a>
                        <span className={classes.gigInfo}>
                          <b className={classes.b}>
                            <a className={classes.a1GigInfo} href="">
                              Web &amp; Mobile Design
                            </a>
                          </b>
                          <a className={classes.a2GigInfo} href="">
                            by&nbsp;skydesigner
                          </a>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.projectCard}>
                      <a className={classes.a} href="">
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 2x"
                          />
                          <img
                            alt="Landing Page Design"
                            className={classes.imgProjectCard}
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 2x"
                            loading="lazy"
                          />
                        </picture>
                      </a>

                      <div className={classes.projectInfo}>
                        <a className={classes.a} href="">
                          <img
                            className={classes.imgProjectInfo}
                            alt="Seller profile image"
                            src="https://fiverr-res.cloudinary.com/t_profile_thumb,q_auto,f_auto/attachments/profile/photo/79cf5c7a560e6668555338b2831480e1-1539770224917/2bb8af3c-4cce-42a8-a699-f11177524084.png"
                            loading="lazy"
                          />
                        </a>
                        <span className={classes.gigInfo}>
                          <b className={classes.b}>
                            <a className={classes.a1GigInfo} href="">
                              Web &amp; Mobile Design
                            </a>
                          </b>
                          <a className={classes.a2GigInfo} href="">
                            by&nbsp;skydesigner
                          </a>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.projectCard}>
                      <a className={classes.a} href="">
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615096/passionshake.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615096/passionshake.jpeg 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615096/passionshake.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615096/passionshake.jpeg 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615096/passionshake.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615096/passionshake.jpeg 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615096/passionshake.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615096/passionshake.jpeg 2x"
                          />
                          <img
                            alt="Product Photography"
                            className={classes.imgProjectCard}
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615096/passionshake.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615096/passionshake.jpeg 2x"
                            loading="lazy"
                          />
                        </picture>
                      </a>

                      <div className={classes.projectInfo}>
                        <a className={classes.a} href="">
                          <img
                            className={classes.imgProjectInfo}
                            alt="Seller profile image"
                            src="https://fiverr-res.cloudinary.com/t_profile_thumb,q_auto,f_auto/attachments/profile/photo/ad68f554913d9d326d611e604ef79b0b-1608722734146/74e5ab33-a5fc-40ae-9cee-a91b23e80237.jpg"
                            loading="lazy"
                          />
                        </a>
                        <span className={classes.gigInfo}>
                          <b className={classes.b}>
                            <a className={classes.a1GigInfo} href="">
                              Product Photography
                            </a>
                          </b>
                          <a className={classes.a2GigInfo} href="">
                            by&nbsp;passionshake
                          </a>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.projectCard}>
                      <a className={classes.a} href="">
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/d51cf66f8a7026eb56a8c8e6b6793b24-1617027896306/lamonastudio-img.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/d51cf66f8a7026eb56a8c8e6b6793b24-1617027896306/lamonastudio-img.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_1.0/v1/attachments/generic_asset/asset/d51cf66f8a7026eb56a8c8e6b6793b24-1617027896306/lamonastudio-img.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_2.0/v1/attachments/generic_asset/asset/d51cf66f8a7026eb56a8c8e6b6793b24-1617027896306/lamonastudio-img.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_1.0/v1/attachments/generic_asset/asset/d51cf66f8a7026eb56a8c8e6b6793b24-1617027896306/lamonastudio-img.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_2.0/v1/attachments/generic_asset/asset/d51cf66f8a7026eb56a8c8e6b6793b24-1617027896306/lamonastudio-img.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/d51cf66f8a7026eb56a8c8e6b6793b24-1617027896306/lamonastudio-img.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/d51cf66f8a7026eb56a8c8e6b6793b24-1617027896306/lamonastudio-img.png 2x"
                          />
                          <img
                            alt="GIF Animation"
                            className={classes.imgProjectCard}
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/d51cf66f8a7026eb56a8c8e6b6793b24-1617027896306/lamonastudio-img.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/d51cf66f8a7026eb56a8c8e6b6793b24-1617027896306/lamonastudio-img.png 2x"
                            loading="lazy"
                          />
                        </picture>
                      </a>

                      <div className={classes.projectInfo}>
                        <a className={classes.a} href="">
                          <img
                            className={classes.imgProjectInfo}
                            alt="Seller profile image"
                            src="https://fiverr-res.cloudinary.com/t_profile_thumb,q_auto,f_auto/attachments/profile/photo/706891a4cc08826adca2819e14129aaf-1583755607494/5a706f4e-9f73-4ebc-97ff-9d2ef16bf28c.jpg"
                            loading="lazy"
                          />
                        </a>
                        <span className={classes.gigInfo}>
                          <b className={classes.b}>
                            <a className={classes.a1GigInfo} href="">
                              Animated GIFs
                            </a>
                          </b>
                          <a className={classes.a2GigInfo} href="">
                            by&nbsp;lamonastudio
                          </a>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.projectCard}>
                      <a className={classes.a} href="">
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/aa2ff6a65708e858cd563bedbc1f9e48-1617004762616/spickex.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/aa2ff6a65708e858cd563bedbc1f9e48-1617004762616/spickex.jpeg 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_1.0/v1/attachments/generic_asset/asset/aa2ff6a65708e858cd563bedbc1f9e48-1617004762616/spickex.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_2.0/v1/attachments/generic_asset/asset/aa2ff6a65708e858cd563bedbc1f9e48-1617004762616/spickex.jpeg 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_1.0/v1/attachments/generic_asset/asset/aa2ff6a65708e858cd563bedbc1f9e48-1617004762616/spickex.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_2.0/v1/attachments/generic_asset/asset/aa2ff6a65708e858cd563bedbc1f9e48-1617004762616/spickex.jpeg 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/aa2ff6a65708e858cd563bedbc1f9e48-1617004762616/spickex.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/aa2ff6a65708e858cd563bedbc1f9e48-1617004762616/spickex.jpeg 2x"
                          />
                          <img
                            alt="Catalog Design"
                            className={classes.imgProjectCard}
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/aa2ff6a65708e858cd563bedbc1f9e48-1617004762616/spickex.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/aa2ff6a65708e858cd563bedbc1f9e48-1617004762616/spickex.jpeg 2x"
                            loading="lazy"
                          />
                        </picture>
                      </a>

                      <div className={classes.projectInfo}>
                        <a className={classes.a} href="">
                          <img
                            className={classes.imgProjectInfo}
                            alt="Seller profile image"
                            src="https://fiverr-res.cloudinary.com/t_profile_thumb,q_auto,f_auto/profile/photos/22711136/original/fiverr_profile.jpg"
                            loading="lazy"
                          />
                        </a>
                        <span className={classes.gigInfo}>
                          <b className={classes.b}>
                            <a className={classes.a1GigInfo} href="">
                              Flyer Design
                            </a>
                          </b>
                          <a className={classes.a2GigInfo} href="">
                            by&nbsp;spickex
                          </a>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.projectCard}>
                      <a className={classes.a} href="">
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/annapietrangeli.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/annapietrangeli.jpeg 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/annapietrangeli.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/annapietrangeli.jpeg 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/annapietrangeli.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/annapietrangeli.jpeg 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/annapietrangeli.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/annapietrangeli.jpeg 2x"
                          />
                          <img
                            alt="Book Cover"
                            className={classes.imgProjectCard}
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/annapietrangeli.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/annapietrangeli.jpeg 2x"
                            loading="lazy"
                          />
                        </picture>
                      </a>

                      <div className={classes.projectInfo}>
                        <a className={classes.a} href="">
                          <img
                            className={classes.imgProjectInfo}
                            alt="Seller profile image"
                            src="https://fiverr-res.cloudinary.com/t_profile_thumb,q_auto,f_auto/attachments/profile/photo/8b60be1bf2915ddc1d551eaa252684d7-1589020928117/1d531e54-7607-4bdb-815f-088dbc0fb971.jpg"
                            loading="lazy"
                          />
                        </a>
                        <span className={classes.gigInfo}>
                          <b className={classes.b}>
                            <a className={classes.a1GigInfo} href="">
                              Book Design
                            </a>
                          </b>
                          <a className={classes.a2GigInfo} href="">
                            by&nbsp;annapietrangeli
                          </a>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.projectCard}>
                      <a className={classes.a} href="">
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615077/eveeelin.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615077/eveeelin.jpeg 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615077/eveeelin.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615077/eveeelin.jpeg 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615077/eveeelin.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615077/eveeelin.jpeg 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615077/eveeelin.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615077/eveeelin.jpeg 2x"
                          />
                          <img
                            alt="Brand Style Guides"
                            className={classes.imgProjectCard}
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615077/eveeelin.jpeg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615077/eveeelin.jpeg 2x"
                            loading="lazy"
                          />
                        </picture>
                      </a>

                      <div className={classes.projectInfo}>
                        <a className={classes.a} href="">
                          <img
                            className={classes.imgProjectInfo}
                            alt="Seller profile image"
                            src="https://fiverr-res.cloudinary.com/t_profile_thumb,q_auto,f_auto/attachments/profile/photo/27bdb82e27e444fe2b27aa7b3083cee8-1591694084918/f79ede47-da5f-440a-bf23-57ed9ef7d363.png"
                            loading="lazy"
                          />
                        </a>
                        <span className={classes.gigInfo}>
                          <b className={classes.b}>
                            <a className={classes.a1GigInfo} href="">
                              Logo Design
                            </a>
                          </b>
                          <a className={classes.a2GigInfo} href="">
                            by&nbsp;eveeelin
                          </a>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.projectCard}>
                      <a className={classes.a} href="">
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615068/mijalzagier.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615068/mijalzagier.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615068/mijalzagier.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615068/mijalzagier.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615068/mijalzagier.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615068/mijalzagier.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615068/mijalzagier.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615068/mijalzagier.png 2x"
                          />
                          <img
                            alt="Product Packaging"
                            className={classes.imgProjectCard}
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615068/mijalzagier.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615068/mijalzagier.png 2x"
                            loading="lazy"
                          />
                        </picture>
                      </a>

                      <div className={classes.projectInfo}>
                        <a className={classes.a} href="">
                          <img
                            className={classes.imgProjectInfo}
                            alt="Seller profile image"
                            src="https://fiverr-res.cloudinary.com/t_profile_thumb,q_auto,f_auto/attachments/profile/photo/58960b09956dc710d2d5a33573261936-1554984111113/750ccab0-8a64-4c91-b9a4-d10039dbf79c.jpg"
                            loading="lazy"
                          />
                        </a>
                        <span className={classes.gigInfo}>
                          <b className={classes.b}>
                            <a className={classes.a1GigInfo} href="">
                              Packaging Design
                            </a>
                          </b>
                          <a className={classes.a2GigInfo} href="">
                            by&nbsp;mijalzagier
                          </a>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.projectCard}>
                      <a className={classes.a} href="">
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615069/noneyn.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615069/noneyn.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615069/noneyn.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615069/noneyn.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615069/noneyn.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615069/noneyn.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615069/noneyn.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615069/noneyn.png 2x"
                          />
                          <img
                            alt="Realistic Portrait"
                            className={classes.imgProjectCard}
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615069/noneyn.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615069/noneyn.png 2x"
                            loading="lazy"
                          />
                        </picture>
                      </a>

                      <div className={classes.projectInfo}>
                        <a className={classes.a} href="">
                          <img
                            className={classes.imgProjectInfo}
                            alt="Seller profile image"
                            src="https://fiverr-res.cloudinary.com/t_profile_thumb,q_auto,f_auto/attachments/profile/photo/23b01eca3b78e2869e149efe15d3066a-1613424545655/0aaffa8e-01e0-4dcb-b56d-674e9b9c4bf5.jpg"
                            loading="lazy"
                          />
                        </a>
                        <span className={classes.gigInfo}>
                          <b className={classes.b}>
                            <a className={classes.a1GigInfo} href="">
                              Portraits &amp; Caricatures
                            </a>
                          </b>
                          <a className={classes.a2GigInfo} href="">
                            by&nbsp;noneyn
                          </a>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.projectCard}>
                      <a className={classes.a} href="">
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/bruno_malagrino.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/bruno_malagrino.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/bruno_malagrino.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/bruno_malagrino.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/bruno_malagrino.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/bruno_malagrino.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/bruno_malagrino.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/bruno_malagrino.png 2x"
                          />
                          <img
                            alt="Logo Design"
                            className={classes.imgProjectCard}
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/bruno_malagrino.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615063/bruno_malagrino.png 2x"
                            loading="lazy"
                          />
                        </picture>
                      </a>

                      <div className={classes.projectInfo}>
                        <a className={classes.a} href="">
                          <img
                            className={classes.imgProjectInfo}
                            alt="Seller profile image"
                            src="https://fiverr-res.cloudinary.com/t_profile_thumb,q_auto,f_auto/attachments/profile/photo/c15f6b22da97be41a8878e753a1a16c2-863645391592368980.489561/AF1BF970-07CA-454B-8AF1-2F3E06838C8B"
                            loading="lazy"
                          />
                        </a>
                        <span className={classes.gigInfo}>
                          <b className={classes.b}>
                            <a className={classes.a1GigInfo} href="">
                              Logo Design
                            </a>
                          </b>
                          <a className={classes.a2GigInfo} href="">
                            by&nbsp;bruno_malagrino
                          </a>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.projectCard}>
                      <a className={classes.a} href="">
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615082/christophbrandl.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615082/christophbrandl.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615082/christophbrandl.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615082/christophbrandl.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615082/christophbrandl.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615082/christophbrandl.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615082/christophbrandl.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615082/christophbrandl.png 2x"
                          />
                          <img
                            alt="Illustration"
                            className={classes.imgProjectCard}
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615082/christophbrandl.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615082/christophbrandl.png 2x"
                            loading="lazy"
                          />
                        </picture>
                      </a>

                      <div className={classes.projectInfo}>
                        <a className={classes.a} href="">
                          <img
                            className={classes.imgProjectInfo}
                            alt="Seller profile image"
                            src="https://fiverr-res.cloudinary.com/t_profile_thumb,q_auto,f_auto/attachments/profile/photo/b615b780b5c813d932953d05ec10f811-1596879215580/6b4a9867-ad06-415f-b307-11177ae30fdd.jpeg"
                            loading="lazy"
                          />
                        </a>
                        <span className={classes.gigInfo}>
                          <b className={classes.b}>
                            <a className={classes.a1GigInfo} href="">
                              Illustration
                            </a>
                          </b>
                          <a className={classes.a2GigInfo} href="">
                            by&nbsp;christophbrandl
                          </a>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.projectCard}>
                      <a className={classes.a} href="">
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615084/fernandobengua.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615084/fernandobengua.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615084/fernandobengua.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615084/fernandobengua.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615084/fernandobengua.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615084/fernandobengua.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615084/fernandobengua.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615084/fernandobengua.png 2x"
                          />
                          <img
                            alt="Social Media Design"
                            className={classes.imgProjectCard}
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615084/fernandobengua.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615084/fernandobengua.png 2x"
                            loading="lazy"
                          />
                        </picture>
                      </a>

                      <div className={classes.projectInfo}>
                        <a className={classes.a} href="">
                          <img
                            className={classes.imgProjectInfo}
                            alt="Seller profile image"
                            src="https://fiverr-res.cloudinary.com/t_profile_thumb,q_auto,f_auto/attachments/profile/photo/0738007a913d22569fe2b049f9259526-1589210299120/db111eb4-c119-42b4-9a1d-9116601f3d22.png"
                            loading="lazy"
                          />
                        </a>
                        <span className={classes.gigInfo}>
                          <b className={classes.b}>
                            <a className={classes.a1GigInfo} href="">
                              Social Media Design
                            </a>
                          </b>
                          <a className={classes.a2GigInfo} href="">
                            by&nbsp;fernandobengua
                          </a>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.projectCard}>
                      <a className={classes.a} href="">
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_300,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_400,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 2x"
                          />
                          <img
                            alt="Landing Page Design"
                            className={classes.imgProjectCard}
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_1.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_320,dpr_2.0/v1/attachments/generic_asset/asset/f23a46693ef0e611430e232cbc989e2b-1617004615106/skydesigner.png 2x"
                            loading="lazy"
                          />
                        </picture>
                      </a>

                      <div className={classes.projectInfo}>
                        <a className={classes.a} href="">
                          <img
                            className={classes.imgProjectInfo}
                            alt="Seller profile image"
                            src="https://fiverr-res.cloudinary.com/t_profile_thumb,q_auto,f_auto/attachments/profile/photo/79cf5c7a560e6668555338b2831480e1-1539770224917/2bb8af3c-4cce-42a8-a699-f11177524084.png"
                            loading="lazy"
                          />
                        </a>
                        <span className={classes.gigInfo}>
                          <b className={classes.b}>
                            <a className={classes.a1GigInfo} href="">
                              Web &amp; Mobile Design
                            </a>
                          </b>
                          <a className={classes.a2GigInfo} href="">
                            by&nbsp;skydesigner
                          </a>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <button
              className={classes.buttonNext}
              id="Next-btn"
              onClick={clickNextOrPrev}
            ></button>
          </div>
        </div>

        <a className={classes.aSeeMoreLink} href="" target="_blank">
          See More Projects
          <span className={classes.spanSeeMoreLink}>
            <svg
              style={{ float: "left", width: "100%", height: "100%" }}
              width={8}
              height={16}
              viewBox="0 0 8 16"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path d="M0.772126 1.19065L0.153407 1.80934C0.00696973 1.95578 0.00696973 2.19322 0.153407 2.33969L5.80025 8L0.153407 13.6603C0.00696973 13.8067 0.00696973 14.0442 0.153407 14.1907L0.772126 14.8094C0.918563 14.9558 1.156 14.9558 1.30247 14.8094L7.84666 8.26519C7.99309 8.11875 7.99309 7.88131 7.84666 7.73484L1.30247 1.19065C1.156 1.04419 0.918563 1.04419 0.772126 1.19065Z" />
            </svg>
          </span>
        </a>
      </div>
    </div>
  );
}
