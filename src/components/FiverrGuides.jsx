import React from "react";
import { makeStyles } from "@mui/styles";

const useStyles = makeStyles({
  guides: {
    display: "flex",
    flex: "0 1 auto",
    flexFrow: "row wrap",
    position: "relative",
    margin: "0 -16px",
  },
  h2: {
    display: "inline-block",
    padding: "0 0 24px",
    fontSize: "32px",
    lineHeight: "120%",
    color: "#404145",
  },
  h6: {
    margin: "0",
    padding: "0 0 2px",
    fontSize: "18px",
    lineHeight: "140%",
    color: "#404145",
  },
  aGuides: {
    padding: "0 16px",
    textDecoration: "none",
    flexBasis: "33.33333%",
    maxWidth: "33.33333%",
    color: "#1dbf73",
    transition: "color .2s",
    "&:hover": { color: "#1dbf73" },
  },
  imgGuides: {
    margin: "0 0 16px",
    borderRadius: "4px",
    width: "100%",
    objectFit: "cover",
    display: "block",
    transition: "opacity .3s ease-in-out",
    "&:hover": {
      opacity: "0.85",
    },
  },
  aSeeMoreLink: {
    fontSize: "16px",
    lineHeight: "24px",
    padding: "10px 0 0",
    top: "0",
    right: "0",
    textDecoration: "none",
    position: "absolute",
    color: "#446ee7",
    display: "block",
  },
  spanSeeMoreLink: {
    width: "10px",
    height: "10px",
    fill: "#446ee7",
    paddingLeft: "8px",
    background: "none",
    border: "none",
    display: "inline-block",
    margin: "0",
    padding: "0",
  },
  signup: {
    padding: "120px 96px",
    backgroundColor: "#45091b",
    borderRadius: "4px",
    overflow: "hidden",
    position: "relative",
    display: "flex",
  },
  imgSignup: {
    width: "100%",
    objectFit: "cover",
    position: "absolute",
    top: "0",
    right: "0",
    height: "100%",
  },
  h2SignupText: {
    maxWidth: "650px",
    color: "#fff",
    padding: "0 0 40px",
    fontSize: "48px",
    lineHeight: "56px",
  },
  aSignupText: {
    textDecoration: "none",
    border: "1px solid transparent",
    borderRadius: "4px",
    boxSizing: "border-box",
    cursor: "pointer",
    display: "inline-block",
    fontSize: "16px",
    fontWeight: "600",
    lineHeight: "100%",
    padding: "12px 24px",
    position: "relative",
    textAlign: "center",
    transition: " 70ms cubic-bezier(.75,0,.25,1)",
    backgroundColor: "#1dbf73",
    color: "#fff",
    "&:hover": {
      background: "#19a463",
      color: "#fff",
    },
  },
});

export default function FiverrGuides() {
  const classes = useStyles();

  return (
    <div
      className="fiverr-guides-container"
      style={{ padding: "0 32px", margin: "0 0 96px" }}
    >
      <div
        className="fiverr-guides"
        style={{ margin: "0 0 96px", position: "relative" }}
      >
        <h2 className={classes.h2}>Fiverr guides</h2>

        <div className={classes.guides}>
          <a href="" className={classes.aGuides} target="_blank">
            <picture>
              <source
                media="(min-width: 1160px)"
                srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_450,dpr_1.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560157/guide-start-online-business-552-x2.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_450,dpr_2.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560157/guide-start-online-business-552-x2.png 2x"
              />
              <source
                media="(min-width: 900px)"
                srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_350,dpr_1.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560157/guide-start-online-business-552-x2.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_350,dpr_2.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560157/guide-start-online-business-552-x2.png 2x"
              />
              <source
                media="(min-width: 600px)"
                srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_410,dpr_1.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560157/guide-start-online-business-552-x2.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_410,dpr_2.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560157/guide-start-online-business-552-x2.png 2x"
              />
              <source
                media="(min-width: 361px)"
                srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_552,dpr_1.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560157/guide-start-online-business-552-x2.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_552,dpr_2.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560157/guide-start-online-business-552-x2.png 2x"
              />
              <source
                media="(max-width: 360px)"
                srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_312,dpr_1.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560157/guide-start-online-business-552-x2.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_312,dpr_2.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560157/guide-start-online-business-552-x2.png 2x"
              />
              <img
                className={classes.imgGuides}
                alt="Start an online business and work from home"
                src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_450,dpr_1.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560157/guide-start-online-business-552-x2.png"
                loading="lazy"
              />
            </picture>
            <h6 className={classes.h6}>
              Start an online business and work from home
            </h6>
            <p style={{ color: "#74767e", margin: "0" }}>
              A complete guide to starting a small business online
            </p>
          </a>
          <a
            href="/resources/guides/digital-marketing/what-is-digital-marketing"
            className={classes.aGuides}
            target="_blank"
          >
            <picture>
              <source
                media="(min-width: 1160px)"
                srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_450,dpr_1.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560168/guide-digital-marketing-552-x2.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_450,dpr_2.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560168/guide-digital-marketing-552-x2.png 2x"
              />
              <source
                media="(min-width: 900px)"
                srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_350,dpr_1.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560168/guide-digital-marketing-552-x2.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_350,dpr_2.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560168/guide-digital-marketing-552-x2.png 2x"
              />
              <source
                media="(min-width: 600px)"
                srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_410,dpr_1.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560168/guide-digital-marketing-552-x2.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_410,dpr_2.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560168/guide-digital-marketing-552-x2.png 2x"
              />
              <source
                media="(min-width: 361px)"
                srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_552,dpr_1.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560168/guide-digital-marketing-552-x2.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_552,dpr_2.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560168/guide-digital-marketing-552-x2.png 2x"
              />
              <source
                media="(max-width: 360px)"
                srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_312,dpr_1.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560168/guide-digital-marketing-552-x2.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_312,dpr_2.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560168/guide-digital-marketing-552-x2.png 2x"
              />
              <img
                className={classes.imgGuides}
                alt="Digital marketing made easy"
                src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_450,dpr_1.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560168/guide-digital-marketing-552-x2.png"
                loading="lazy"
              />
            </picture>
            <h6 className={classes.h6}>Digital marketing made easy</h6>
            <p style={{ color: "#74767e", margin: "0" }}>
              A practical guide to understand what is digital marketing
            </p>
          </a>
          <a
            href="/resources/guides/graphic-design/how-to-create-business-logo"
            className={classes.aGuides}
            target="_blank"
          >
            <picture>
              <source
                media="(min-width: 1160px)"
                srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_450,dpr_1.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560155/guide-create-a-logo-552-x2.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_450,dpr_2.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560155/guide-create-a-logo-552-x2.png 2x"
              />
              <source
                media="(min-width: 900px)"
                srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_350,dpr_1.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560155/guide-create-a-logo-552-x2.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_350,dpr_2.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560155/guide-create-a-logo-552-x2.png 2x"
              />
              <source
                media="(min-width: 600px)"
                srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_410,dpr_1.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560155/guide-create-a-logo-552-x2.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_410,dpr_2.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560155/guide-create-a-logo-552-x2.png 2x"
              />
              <source
                media="(min-width: 361px)"
                srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_552,dpr_1.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560155/guide-create-a-logo-552-x2.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_552,dpr_2.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560155/guide-create-a-logo-552-x2.png 2x"
              />
              <source
                media="(max-width: 360px)"
                srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_312,dpr_1.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560155/guide-create-a-logo-552-x2.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_312,dpr_2.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560155/guide-create-a-logo-552-x2.png 2x"
              />
              <img
                className={classes.imgGuides}
                alt="Create a logo for your business"
                src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_450,dpr_1.0/v1/attachments/generic_asset/asset/d532c0ad4feed007b3899cebad595286-1599611560155/guide-create-a-logo-552-x2.png"
                loading="lazy"
              />
            </picture>
            <h6 className={classes.h6}>Create a logo for your business</h6>
            <p style={{ color: "#74767e", margin: "0" }}>
              A step-by-step guide to create a memorable business logo
            </p>
          </a>
        </div>

        <a className={classes.aSeeMoreLink} href="" target="_blank">
          See More Guides
          <span
            className={classes.spanSeeMoreLink}
            aria-hidden="true"
            style={{ width: 10, height: 10 }}
          >
            <svg
              style={{ float: "left", width: "100%", height: "100%" }}
              width={8}
              height={16}
              viewBox="0 0 8 16"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path d="M0.772126 1.19065L0.153407 1.80934C0.00696973 1.95578 0.00696973 2.19322 0.153407 2.33969L5.80025 8L0.153407 13.6603C0.00696973 13.8067 0.00696973 14.0442 0.153407 14.1907L0.772126 14.8094C0.918563 14.9558 1.156 14.9558 1.30247 14.8094L7.84666 8.26519C7.99309 8.11875 7.99309 7.88131 7.84666 7.73484L1.30247 1.19065C1.156 1.04419 0.918563 1.04419 0.772126 1.19065Z" />
            </svg>
          </span>
        </a>
      </div>

      <div className={classes.signup}>
        <picture className="signup-image">
          <source
            media="(min-width: 1160px)"
            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_1400,dpr_1.0/v1/attachments/generic_asset/asset/50218c41d277f7d85feeaf3efb4549bd-1599072608122/bg-signup-1400-x1.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_1400,dpr_2.0/v1/attachments/generic_asset/asset/50218c41d277f7d85feeaf3efb4549bd-1599072608129/bg-signup-1400-x2.png 2x"
          />
          <source
            media="(min-width: 900px)"
            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_1160,dpr_1.0/v1/attachments/generic_asset/asset/50218c41d277f7d85feeaf3efb4549bd-1599072608114/bg-signup-1160-x1.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_1160,dpr_2.0/v1/attachments/generic_asset/asset/50218c41d277f7d85feeaf3efb4549bd-1599072608134/bg-signup-1160-x2.png 2x"
          />
          <img
            className={classes.imgSignup}
            alt="The talent you need"
            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_1400,dpr_1.0/v1/attachments/generic_asset/asset/50218c41d277f7d85feeaf3efb4549bd-1599072608122/bg-signup-1400-x1.png"
            loading="lazy"
          />
        </picture>

        <div className="signup-text" style={{ zIndex: "5" }}>
          <h2 className={classes.h2SignupText}>
            <span>
              Find the <i style={{ fontWeight: "500" }}>talent</i> needed to get
              your business <i style={{ fontWeight: "500" }}>growing</i>.
            </span>
          </h2>
          <a className={classes.aSignupText} href="">
            Get Started
          </a>
        </div>
      </div>
    </div>
  );
}
