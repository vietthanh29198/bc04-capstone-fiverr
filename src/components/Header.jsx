import React from "react";
import { makeStyles } from "@mui/styles";
import AutoCarousel from "./AutoCarousel";
import SeacrhBar from "./SeacrhBar";
import Popular from "./Popular";

const useStyles = makeStyles({
  heroWrapper: {
    height: "680px",
    backgroundColor: "#023a15",
    position: "relative",
    margin: "0",
  },
  hero: {
    display: "flex",
    alignItems: "center",
    position: "absolute",
    top: "0",
    left: "0",
    bottom: "0",
    right: "0",
    zIndex: "2",
    padding: "32px 32px 64px",
    margin: "0",
    width: "100%",
  },
  h1: {
    padding: "0 0 24px",
    color: "#fff",
    fontSize: "48px",
    lineHeight: "56px",
    fontWeight: "700",
  },
});

export default function Header() {
  const classes = useStyles();

  return (
    <div className={classes.heroWrapper}>
      <AutoCarousel />
      <div className={classes.hero}>
        <div className="header" style={{ maxWidth: "650px", padding: "0" }}>
          <h1 className={classes.h1}>
            <span>
              Find the perfect <i style={{ fontWeight: "500" }}>freelance</i>{" "}
              services for your business
            </span>
          </h1>
          <SeacrhBar />
          <Popular />
        </div>
      </div>
    </div>
  );
}
