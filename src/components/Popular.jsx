import React from "react";
import { makeStyles } from "@mui/styles";

const useStyles = makeStyles({
  popular: {
    display: "flex",
    fontSize: "14px",
    lineHeight: "27px",
    fontWeight: "600",
    color: "#fff",
    padding: "24px 0 0",
  },
  ul: {
    display: "flex",
    width: "100%",
    margin: "0 0 0 12px",
    listStyle: "none",
    listStyleImage: "none",
    padding: "0",
  },
  li: {
    display: "flex",
    marginRight: "12px",
    whiteSpace: "nowrap",
  },
  textBody: {
    backgroundColor: "transparent",
    lineHeight: "24px",
    fontWeight: "600",
    color: "#fff",
    padding: "1px 12px 0",
    border: "1px solid #fff",
    borderRadius: "40px",
    transition: "all 0.2s ease-in-out",
    zIndex: "4",
    textDecoration: "none",
    fontSize: "14px",
    "&:hover": {
      color: "black",
      background: "white",
    },
  },
});

export default function Popular() {
  const classes = useStyles();

  return (
    <div className={classes.popular}>
      Popular:{" "}
      <ul className={classes.ul}>
        <li className={classes.li}>
          <a
            href="/categories/graphics-design/website-design?source=hplo_search_tag&pos=1&name=website-design"
            className={classes.textBody}
          >
            Website Design
          </a>
        </li>
        <li className={classes.li}>
          <a
            href="/categories/programming-tech/wordpress-services?source=hplo_search_tag&pos=2&name=wordpress-services"
            className={classes.textBody}
          >
            WordPress
          </a>
        </li>
        <li className={classes.li}>
          <a
            href="/categories/graphics-design/creative-logo-design?source=hplo_search_tag&pos=3&name=creative-logo-design"
            className={classes.textBody}
          >
            Logo Design
          </a>
        </li>
        <li className={classes.li}>
          <a
            href="/categories/video-animation/video-editing?source=hplo_search_tag&pos=4&name=video-editing"
            className={classes.textBody}
          >
            Video Editing
          </a>
        </li>
      </ul>
    </div>
  );
}
