import { FormControl, TextField, Typography } from "@mui/material";
import { makeStyles } from "@mui/styles";
import React from "react";
import { Link } from "react-router-dom";

const useStyles = makeStyles({
  column1: {
    float: "left",
    width: "75%",
    height: "100vh",
  },
  column2: {
    float: "right",
    width: "25%",
    height: "100vh",
  },
  row: {
    "&:after": {
      height: "100vh",
      content: "",
      display: "table",
      clear: "both",
    },
  },
  button: {
    marginRight: "4px",
    cursor: "pointer",
    fontSize: "18px",
    lineHeight: "100%",
    textAlign: "center",
    width: "30%",
    height: "48px",
    borderRadius: "30px",
    border: "0",
    boxShadow: "0 2px 5px 0 rgb(0 0 0 / 15%)",
    backgroundColor: "#1DBF73",
    color: "#fff",
    "&:hover": {
      backgroundColor: "#fff",
      color: "#1DBF73",
    },
  },
});

export default function LoginPage() {
  const classes = useStyles();

  return (
    <div
      style={{
        width: "100%",
        height: "100vh",
        position: "relative",
        overflow: "hidden",
      }}
    >
      <div className={classes.row}>
        <div className={classes.column1}>
          <div className="background">
            <img
              style={{
                width: "75vw",
                height: "100vh",
                backgroundRepeat: "no-repeat",
                // backgroundSize: "cover",
              }}
              src="https://fiverr-res.cloudinary.com/image/upload/f_auto,q_auto/v1/attachments/generic_asset/asset/5c837aac7c42de1f9f125cff37ab2c70-1612076004546/fiverr-og-logo.png"
              alt=""
            />
          </div>
        </div>

        <div className={classes.column2}>
          <div
            className="login"
            style={{
              paddingLeft: "10px",
              marginTop: "50%",
            }}
          >
            <Typography className="title">LOGIN</Typography>
            <br></br>
            <FormControl>
              <TextField id="txt-username" type={"text"} label="Username" />
              <br></br>
              <TextField id="txt-password" type={"password"} label="Password" />
              <br></br>
              <div style={{ display: "inline-block" }}>
                <button className={classes.button}>Login</button>
                <Link to="/signin">
                  <button className={classes.button}>Sigin</button>
                </Link>
              </div>
            </FormControl>
          </div>
        </div>
      </div>
    </div>
  );
}
