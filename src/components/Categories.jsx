import React from "react";
import { makeStyles } from "@mui/styles";

const useStyles = makeStyles({
  h2: {
    padding: "0 0 24px",
    margin: "0 0 40px",
    color: "#404145",
    fontSize: "32px",
  },
  ul: {
    display: "flex",
    flexWrap: "wrap",
    listStyle: "none",
    listStyleImage: "none",
    margin: "0",
    padding: "0",
  },
  li: {
    width: "20%",
    padding: "0 10px 55px",
    boxSizing: "border-box",
    textAlign: "center",
  },
  a: {
    color: "#222325",
    textAlign: "center",
    position: "relative",
    display: "inline-block",
    textDecoration: "none",
    transition: "color .2s",
    "&::after": {
      content: '""',
      position: "absolute",
      top: "45px",
      right: "50%",
      transform: "translateX(50%)",
      padding: "0 0 8px",
      width: "48px",
      borderBottom: "2px solid #c5c6c9",
      transition: " border-color .2s ease-in-out,padding .2s ease-in-out",
    },
    "&:hover::after": {
      padding: "0 20px 8px",
      borderColor: "#1dbf73",
    },
  },
  img: {
    display: "block",
    width: "48px",
    height: "48px",
    margin: "0 auto 15px",
  },
});

export default function Categories() {
  const classes = useStyles();

  return (
    <div
      className="main-categories"
      style={{ marginBottom: "96px", padding: "0 32px" }}
    >
      <h2 className={classes.h2}>Explore the marketplace</h2>

      <ul className={classes.ul}>
        <li className={classes.li}>
          <a
            className={classes.a}
            href="/categories/graphics-design?source=hplo_cat_sec&pos=1"
          >
            <img
              className={classes.img}
              src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/graphics-design.d32a2f8.svg"
              alt="Graphics & Design"
              loading="lazy"
            />
            Graphics &amp; Design
          </a>
        </li>

        <li className={classes.li}>
          <a
            className={classes.a}
            href="/categories/online-marketing?source=hplo_cat_sec&pos=2"
          >
            <img
              className={classes.img}
              src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/online-marketing.74e221b.svg"
              alt="Digital Marketing"
              loading="lazy"
            />
            Digital Marketing
          </a>
        </li>

        <li className={classes.li}>
          <a
            className={classes.a}
            href="/categories/writing-translation?source=hplo_cat_sec&pos=3"
          >
            <img
              className={classes.img}
              src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/writing-translation.32ebe2e.svg"
              alt="Writing & Translation"
              loading="lazy"
            />
            Writing &amp; Translation
          </a>
        </li>

        <li className={classes.li}>
          <a
            className={classes.a}
            href="/categories/video-animation?source=hplo_cat_sec&pos=4"
          >
            <img
              className={classes.img}
              src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/video-animation.f0d9d71.svg"
              alt="Video & Animation"
              loading="lazy"
            />
            Video &amp; Animation
          </a>
        </li>

        <li className={classes.li}>
          <a
            className={classes.a}
            href="/categories/music-audio?source=hplo_cat_sec&pos=5"
          >
            <img
              className={classes.img}
              src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/music-audio.320af20.svg"
              alt="Music & Audio"
              loading="lazy"
            />
            Music &amp; Audio
          </a>
        </li>

        <li className={classes.li}>
          <a
            className={classes.a}
            href="/categories/programming-tech?source=hplo_cat_sec&pos=6"
          >
            <img
              className={classes.img}
              src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/programming.9362366.svg"
              alt="Programming & Tech"
              loading="lazy"
            />
            Programming &amp; Tech
          </a>
        </li>

        <li className={classes.li}>
          <a
            className={classes.a}
            href="/categories/business?source=hplo_cat_sec&pos=7"
          >
            <img
              className={classes.img}
              src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/business.bbdf319.svg"
              alt="Business"
              loading="lazy"
            />
            Business
          </a>
        </li>

        <li className={classes.li}>
          <a
            className={classes.a}
            href="/categories/lifestyle?source=hplo_cat_sec&pos=8"
          >
            <img
              className={classes.img}
              src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/lifestyle.745b575.svg"
              alt="Lifestyle"
              loading="lazy"
            />
            Lifestyle
          </a>
        </li>

        <li className={classes.li}>
          <a
            className={classes.a}
            href="/categories/data?source=hplo_cat_sec&pos=9"
          >
            <img
              className={classes.img}
              src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/data.718910f.svg"
              alt="Data"
              loading="lazy"
            />
            Data
          </a>
        </li>
      </ul>
    </div>
  );
}
