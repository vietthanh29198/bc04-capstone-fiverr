import { makeStyles } from "@mui/styles";
import { useEffect, useState } from "react";
import React from "react";

const useStyles = makeStyles({
  heroAndrea: {
    backgroundColor: "#023a15",
    backgroundPosition: "bottom",
    display: "flex",
    justifyContent: "flex-end",
    backgroundRepeat: "no-repeat",
    backgroundSize: "auto 100%",
    position: "absolute",
    top: "0",
    left: "0",
    bottom: "0",
    right: "0",
    zIndex: "1",
    animation: "fade 2s linear",
    transition: "opacity 0.9s",
  },
  heroMoon: {
    backgroundColor: "#b64762",
    backgroundPosition: "bottom",
    display: "flex",
    justifyContent: "flex-end",
    backgroundRepeat: "no-repeat",
    backgroundSize: "auto 100%",
    position: "absolute",
    top: "0",
    left: "0",
    bottom: "0",
    right: "0",
    zIndex: "1",
    animation: "fade 2s linear",
    transition: "opacity 0.9s",
  },
  heroRitika: {
    backgroundColor: "#540e1f",
    backgroundPosition: "bottom",
    display: "flex",
    justifyContent: "flex-end",
    backgroundRepeat: "no-repeat",
    backgroundSize: "auto 100%",
    position: "absolute",
    top: "0",
    left: "0",
    bottom: "0",
    right: "0",
    zIndex: "1",
    animation: "fade 2s linear",
    transition: "opacity 0.9s",
  },
  heroZach: {
    backgroundColor: "#023a15",
    backgroundPosition: "bottom",
    display: "flex",
    justifyContent: "flex-end",
    backgroundRepeat: "no-repeat",
    backgroundSize: "auto 100%",
    position: "absolute",
    top: "0",
    left: "0",
    bottom: "0",
    right: "0",
    zIndex: "1",
    animation: "fade 2s linear",
    transition: "opacity 0.9s",
  },
  heroGabrielle: {
    backgroundColor: "#7d1a00",
    backgroundPosition: "bottom",
    display: "flex",
    justifyContent: "flex-end",
    backgroundRepeat: "no-repeat",
    backgroundSize: "auto 100%",
    position: "absolute",
    top: "0",
    left: "0",
    bottom: "0",
    right: "0",
    zIndex: "1",
    animation: "fade 2s linear",
    transition: "opacity 0.9s",
  },
  sellerName: {
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "flex-end",
    whiteSpace: "pre-line",
    padding: "32px 32px 64px",
    margin: "0",
    boxSizing: "border-box",
    width: "100%",
    position: "absolute",
    bottom: "-10%",
    right: "10%",
  },
  p: {
    color: "#fff",
    display: "inline-block",
    paddingBottom: "32px",
  },
  pStar: {
    color: "#fff",
    display: "inline-block",
    paddingBottom: "32px",
    "&::before": {
      content: '""',
      backgroundSize: "77px",
      background:
        "url(https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/five_stars.e5c37f5.svg) ",
      backgroundRepeat: "no-repeat",
      display: "block",
      height: "17px",
    },
  },
});

export default function AutoCarousel() {
  const classes = useStyles();
  var isCarouselLooping = true;

  var slideIndex = 0;

  useEffect(() => {
    carousel();

    return () => {
      isCarouselLooping = false;
    };
  }, []);

  function carousel() {
    if (!isCarouselLooping) return;
    var i;
    var x = document.getElementsByClassName("auto-carousel");

    for (i = 0; i < x.length; i++) {
      x[i].style.display = "none";
    }

    slideIndex++;
    if (slideIndex > x.length) {
      slideIndex = 1;
    }
    x[slideIndex - 1].style.display = "block";

    if (isCarouselLooping) setTimeout(carousel, 8000);
  }
  return (
    <div className="hero-backgrounds">
      <div className="auto-carousel">
        <div className={classes.heroAndrea}>
          <img src="https://fiverr-res.cloudinary.com/image/upload/q_auto,f_auto/v1/attachments/generic_asset/asset/bb5958e41c91bb37f4afe2a318b71599-1599344049983/bg-hero-1-1792-x1.png" />
          <div className={classes.sellerName}>
            <p className={classes.p}>
              Andrea, <b>Fashion Designer</b>
            </p>
          </div>
        </div>
      </div>

      <div className="auto-carousel">
        <div className={classes.heroMoon}>
          <img src="https://fiverr-res.cloudinary.com/image/upload/q_auto,f_auto/v1/attachments/generic_asset/asset/2413b8415dda9dbd7756d02cb87cd4b1-1599595203045/bg-hero-2-1792-x1.png" />
          <div className={classes.sellerName}>
            <p className={classes.pStar}>
              Moon, <b>Marketing Expert</b>
            </p>
          </div>
        </div>
      </div>

      <div className="auto-carousel">
        <div className={classes.heroRitika}>
          <img src="https://fiverr-res.cloudinary.com/image/upload/q_auto,f_auto/v1/attachments/generic_asset/asset/d14871e2d118f46db2c18ad882619ea8-1599835783966/bg-hero-3-1792-x1.png" />
          <div className={classes.sellerName}>
            <p className={classes.p}>
              Ritika, <b>Shoemaker and Designer</b>
            </p>
          </div>
        </div>
      </div>

      <div className="auto-carousel">
        <div className={classes.heroZach}>
          <img src="https://fiverr-res.cloudinary.com/image/upload/q_auto,f_auto/v1/attachments/generic_asset/asset/93085acc959671e9e9e77f3ca8147f82-1599427734108/bg-hero-4-1792-x1.png" />
          <div className={classes.sellerName}>
            <p className={classes.p}>
              Zach, <b>Bar Owner</b>
            </p>
          </div>
        </div>
      </div>

      <div className="auto-carousel">
        <div className={classes.heroGabrielle}>
          <img src="https://fiverr-res.cloudinary.com/image/upload/q_auto,f_auto/v1/attachments/generic_asset/asset/bb5958e41c91bb37f4afe2a318b71599-1599344049970/bg-hero-5-1792-x1.png" />
          <div className={classes.sellerName}>
            <p className={classes.pStar}>
              Gabrielle, <b>Video Editor</b>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
