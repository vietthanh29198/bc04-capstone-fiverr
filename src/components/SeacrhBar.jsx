import { makeStyles } from "@mui/styles";
import React from "react";

const useStyles = makeStyles({
  searchForm: {
    flexDirection: "row",
    height: "auto",
    display: "flex",
    position: "relative",
  },
  searchBarIcon: {
    width: "16px",
    height: "16px",
    top: "16px",
    left: "16px",
    position: "absolute",
    zIndex: "5",
    fill: "#74767e",
    background: "none",
    border: "none",
    display: "inline-block",
    margin: "0",
    padding: "0",
  },
  input: {
    backgroundColor: "#fff",
    borderRadius: "4px 0 0 4px",
    fontSize: "16px",
    height: "48px",
    padding: "8px 7px 7px 44px",
    margin: "0",
    borderRight: "0",
    flex: "1 1 auto",
    borderColor: "#dadbdd",
    boxShadow: "none",
    zIndex: "3",
    border: "1px solid #c5c6c9",
    boxSizing: "border-box",
    color: "#404145",
    font: "inherit",
    resize: "none",
    transition: "0.15s ease-in-out",
    width: "100%",
  },
  submitButton: {
    margin: "0",
    borderRadius: "0 4px 4px 0",
    display: "block",
    fontSize: "16px",
    height: "48px",
    padding: "12px 24px",
    fontWeight: "700",
    flexShrink: "0",
    zIndex: "3",
    border: "1px solid transparent",
    boxSizing: "border-box",
    cursor: "pointer",
    lineHeight: "100%",
    position: "relative",
    textAlign: "center",
    textDecoration: "none",
    transition: "70ms cubic-bezier(.75,0,.25,1)",
    backgroundColor: "#1dbf73",
    color: "#fff",
    "&:hover": {
      backgroundColor: "#19a463",
    },
  },
});

export default function SeacrhBar() {
  const classes = useStyles();

  return (
    <div
      className="search-bar-package search_bar-package"
      style={{ display: "block", zIndex: "5", positon: "relative" }}
    >
      <form className={classes.searchForm}>
        <span
          className={classes.searchBarIcon}
          aria-hidden="true"
          style={{ width: 16, height: 16 }}
        >
          <svg
            style={{ float: "left", width: "100%", height: "100%" }}
            width={16}
            height={16}
            viewBox="0 0 16 16"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path d="M15.8906 14.6531L12.0969 10.8594C12.025 10.7875 11.9313 10.75 11.8313 10.75H11.4187C12.4031 9.60938 13 8.125 13 6.5C13 2.90937 10.0906 0 6.5 0C2.90937 0 0 2.90937 0 6.5C0 10.0906 2.90937 13 6.5 13C8.125 13 9.60938 12.4031 10.75 11.4187V11.8313C10.75 11.9313 10.7906 12.025 10.8594 12.0969L14.6531 15.8906C14.8 16.0375 15.0375 16.0375 15.1844 15.8906L15.8906 15.1844C16.0375 15.0375 16.0375 14.8 15.8906 14.6531ZM6.5 11.5C3.7375 11.5 1.5 9.2625 1.5 6.5C1.5 3.7375 3.7375 1.5 6.5 1.5C9.2625 1.5 11.5 3.7375 11.5 6.5C11.5 9.2625 9.2625 11.5 6.5 11.5Z" />
          </svg>
        </span>
        <input
          className={classes.input}
          type="search"
          autoComplete="off"
          placeholder='Try "building mobile app"'
        />
        <button className={classes.submitButton}>Search</button>
      </form>
    </div>
  );
}
