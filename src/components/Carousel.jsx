import React from "react";
import { makeStyles } from "@mui/styles";

const useStyles = makeStyles({
  sliderPackage: { border: "0", margin: "0", padding: "0", outline: "0" },
  slickSlider: {
    margin: "0 -18px",
    position: "relative",
    display: "block",
    boxSizing: "border-box",
    userSelect: "none",
    touchAction: "pan-y",
  },
  slickList: {
    display: "block",
    position: "relative",
    overflow: "hidden",
    margin: "0",
    padding: "0",
    transform: "translateZ(0)",
  },
  buttonPrev: {
    cursor: "pointer",
    top: "50%",
    fontSize: "16px",
    lineHeight: "100%",
    textAlign: "center",
    left: "-8px",
    position: "absolute",
    zIndex: "4",
    width: "48px",
    height: "48px",
    borderRadius: "30px",
    border: "0",
    boxShadow: "0 2px 5px 0 rgb(0 0 0 / 15%)",
    backgroundColor: "white",
    "&::after": {
      content: `''`,
      display: "inline-block",
      backgroundImage: `url("https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/arrows.a8b0bad.svg")`,
      backgroundRepeat: "no-repeat",
      backgroundSize: "16px 64px",
      width: "16px",
      height: "16px",
      marginTop: "3px",
    },
  },
  slickTrack: {
    display: "flex",
    justifyContent: "flex-start",
    position: "relative",
    left: "0px",
    transition: "left 500ms ease 0s",
    width: "7488px",
    opacity: "1",
    minWidth: "100%",
  },
  slickSlide: {
    width: "282px",
    padding: "0 18px",
    float: "left",
    height: "100%",
    minHeight: "1px",
  },
  subcategoryWrapper: { height: "345px", backgroundColor: "#efeff0" },
  aSubcategory: {
    fontSize: "16px",
    lineHeight: "24px",
    position: "relative",
    display: "block",
    textDecoration: "none",
    color: "#1dbf73",
    transition: "color .2s",
    "&:hover": { color: "#1dbf73" },
  },
  buttonNext: {
    cursor: "pointer",
    top: "50%",
    fontSize: "16px",
    lineHeight: "100%",
    textAlign: "center",
    right: "-8px",
    position: "absolute",
    zIndex: "4",
    width: "48px",
    height: "48px",
    borderRadius: "30px",
    border: "0",
    boxShadow: "0 2px 5px 0 rgb(0 0 0 / 15%)",
    backgroundColor: "white",
    "&::after": {
      content: `''`,
      display: "inline-block",
      backgroundImage: `url("https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/arrows.a8b0bad.svg")`,
      backgroundRepeat: "no-repeat",
      backgroundSize: "16px 64px",
      width: "16px",
      height: "16px",
      marginTop: "3px",
      backgroundPosition: "0 -48px",
    },
  },
  h2: {
    margin: "0",
    color: "#404145",
    padding: "0 0 24px",
    fontSize: "32px",
    lineHeight: "120%",
  },
  h4: {
    position: "relative",
    zIndex: "1",
    color: "#fff",
    padding: "16px",
    fontSize: "24px",
    lineHeight: "130%",
  },
  small: {
    fontSize: "14px",
    lineHeight: "20px",
    fontWeight: "400",
    display: "block",
  },
  img: {
    objectFit: "cover",
    height: "345px",
    width: "100%",
    borderRadius: "4px",
    transition: "opacity .3s ease-in-out",
    position: "absolute",
    top: "0",
    "&:hover": {
      opacity: "0.85",
    },
  },
});

export default function Carousel() {
  const classes = useStyles();

  function clickNextOrPrev() {
    var listItems = document.getElementById("slick-track-carousel");

    listItems.style.left =
      listItems.style.left == "0px" || listItems.style.left == "-1440px"
        ? listItems.style.left.substring(0, listItems.style.left.length - 2) -
          1440 +
          "px"
        : "0px";
  }

  return (
    <div
      className="carousel-container"
      style={{ padding: "0 32px", margin: "0" }}
    >
      <div
        className="subcategory-carousel"
        style={{
          marginBottom: "96px",
        }}
      >
        <h2 className={classes.h2}>Popular professional services</h2>

        <div className={classes.sliderPackage}>
          <div className={classes.slickSlider}>
            <button
              className={classes.buttonPrev}
              id="Previous-btn"
              onClick={clickNextOrPrev}
            ></button>

            <div className={classes.slickList}>
              <div className={classes.slickTrack} id="slick-track-carousel">
                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.subcategoryWrapper}>
                      <a
                        href="/categories/graphics-design/creative-logo-design?source=hplo_subcat_first_step&pos=1"
                        className={classes.aSubcategory}
                      >
                        <h4 className={classes.h4}>
                          <small className={classes.small}>
                            Build your brand
                          </small>
                          Logo Design
                        </h4>
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/logo-design-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/logo-design-2x.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/logo-design-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/logo-design-2x.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/logo-design-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/logo-design-2x.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/logo-design-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/logo-design-2x.png 2x"
                          />
                          <img
                            className={classes.img}
                            alt="Logo Design"
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/logo-design-2x.png"
                          />
                        </picture>
                      </a>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.subcategoryWrapper}>
                      <a
                        href="/categories/programming-tech/wordpress-services?source=hplo_subcat_first_step&pos=2"
                        className={classes.aSubcategory}
                      >
                        <h4 className={classes.h4}>
                          <small className={classes.small}>
                            Customize your site
                          </small>
                          WordPress
                        </h4>
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_1.0/v1/attachments/generic_asset/asset/ae11e2d45410b0eded7fba0e46b09dbd-1598561917003/wordpress-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_2.0/v1/attachments/generic_asset/asset/ae11e2d45410b0eded7fba0e46b09dbd-1598561917003/wordpress-2x.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_1.0/v1/attachments/generic_asset/asset/ae11e2d45410b0eded7fba0e46b09dbd-1598561917003/wordpress-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_2.0/v1/attachments/generic_asset/asset/ae11e2d45410b0eded7fba0e46b09dbd-1598561917003/wordpress-2x.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_1.0/v1/attachments/generic_asset/asset/ae11e2d45410b0eded7fba0e46b09dbd-1598561917003/wordpress-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_2.0/v1/attachments/generic_asset/asset/ae11e2d45410b0eded7fba0e46b09dbd-1598561917003/wordpress-2x.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/ae11e2d45410b0eded7fba0e46b09dbd-1598561917003/wordpress-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/ae11e2d45410b0eded7fba0e46b09dbd-1598561917003/wordpress-2x.png 2x"
                          />
                          <img
                            className={classes.img}
                            alt="WordPress"
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/ae11e2d45410b0eded7fba0e46b09dbd-1598561917003/wordpress-2x.png"
                          />
                        </picture>
                      </a>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.subcategoryWrapper}>
                      <a
                        href="/categories/music-audio/voice-overs?source=hplo_subcat_first_step&pos=3"
                        className={classes.aSubcategory}
                      >
                        <h4 className={classes.h4}>
                          <small className={classes.small}>
                            Share your message
                          </small>
                          Voice Over
                        </h4>
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741669/voiceover-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741669/voiceover-2x.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741669/voiceover-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741669/voiceover-2x.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741669/voiceover-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741669/voiceover-2x.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741669/voiceover-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741669/voiceover-2x.png 2x"
                          />
                          <img
                            className={classes.img}
                            alt="Voice Over"
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741669/voiceover-2x.png"
                          />
                        </picture>
                      </a>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.subcategoryWrapper}>
                      <a
                        href="/categories/video-animation/animated-explainer-videos?source=hplo_subcat_first_step&pos=4"
                        className={classes.aSubcategory}
                      >
                        <h4 className={classes.h4}>
                          <small className={classes.small}>
                            Engage your audience
                          </small>
                          Video Explainer
                        </h4>
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741663/animated-explainer-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741663/animated-explainer-2x.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741663/animated-explainer-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741663/animated-explainer-2x.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741663/animated-explainer-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741663/animated-explainer-2x.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741663/animated-explainer-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741663/animated-explainer-2x.png 2x"
                          />
                          <img
                            className={classes.img}
                            alt="Video Explainer"
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741663/animated-explainer-2x.png"
                          />
                        </picture>
                      </a>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.subcategoryWrapper}>
                      <a
                        href="/categories/online-marketing/social-marketing?source=hplo_subcat_first_step&pos=5"
                        className={classes.aSubcategory}
                      >
                        <h4 className={classes.h4}>
                          <small className={classes.small}>
                            Reach more customers
                          </small>
                          Social Media
                        </h4>
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741667/social-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741667/social-2x.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741667/social-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741667/social-2x.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741667/social-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741667/social-2x.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741667/social-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741667/social-2x.png 2x"
                          />
                          <img
                            className={classes.img}
                            alt="Social Media"
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741667/social-2x.png"
                          />
                        </picture>
                      </a>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.subcategoryWrapper}>
                      <a
                        href="/categories/online-marketing/seo-services?source=hplo_subcat_first_step&pos=6"
                        className={classes.aSubcategory}
                      >
                        <h4 className={classes.h4}>
                          <small className={classes.small}>
                            Unlock growth online
                          </small>
                          SEO
                        </h4>
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741668/seo-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741668/seo-2x.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741668/seo-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741668/seo-2x.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741668/seo-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741668/seo-2x.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741668/seo-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741668/seo-2x.png 2x"
                          />
                          <img
                            className={classes.img}
                            alt="SEO"
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741668/seo-2x.png"
                          />
                        </picture>
                      </a>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.subcategoryWrapper}>
                      <a
                        href="/categories/graphics-design/digital-illustration?source=hplo_subcat_first_step&pos=7"
                        className={classes.aSubcategory}
                      >
                        <h4 className={classes.h4}>
                          <small className={classes.small}>
                            Color your dreams
                          </small>
                          Illustration
                        </h4>
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/illustration-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/illustration-2x.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/illustration-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/illustration-2x.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/illustration-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/illustration-2x.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/illustration-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/illustration-2x.png 2x"
                          />
                          <img
                            className={classes.img}
                            alt="Illustration"
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/illustration-2x.png"
                          />
                        </picture>
                      </a>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.subcategoryWrapper}>
                      <a
                        href="/categories/writing-translation/quality-translation-services?source=hplo_subcat_first_step&pos=8"
                        className={classes.aSubcategory}
                      >
                        <h4 className={classes.h4}>
                          <small className={classes.small}>Go global</small>
                          Translation
                        </h4>
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741674/translation-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741674/translation-2x.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741674/translation-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741674/translation-2x.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741674/translation-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741674/translation-2x.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741674/translation-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741674/translation-2x.png 2x"
                          />
                          <img
                            className={classes.img}
                            alt="Translation"
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741674/translation-2x.png"
                          />
                        </picture>
                      </a>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.subcategoryWrapper}>
                      <a
                        href="/categories/data/data-entry?source=hplo_subcat_first_step&pos=9"
                        className={classes.aSubcategory}
                      >
                        <h4 className={classes.h4}>
                          <small className={classes.small}>
                            Learn your business
                          </small>
                          Data Entry
                        </h4>
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/data-entry-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/data-entry-2x.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/data-entry-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/data-entry-2x.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/data-entry-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/data-entry-2x.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/data-entry-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/data-entry-2x.png 2x"
                          />
                          <img
                            className={classes.img}
                            alt="Data Entry"
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/data-entry-2x.png"
                          />
                        </picture>
                      </a>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.subcategoryWrapper}>
                      <a
                        href="/categories/graphics-design/book-design/cover?source=hplo_subcat_first_step&pos=10"
                        className={classes.aSubcategory}
                      >
                        <h4 className={classes.h4}>
                          <small className={classes.small}>
                            Showcase your story
                          </small>
                          Book Covers
                        </h4>
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/book-covers-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/book-covers-2x.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/book-covers-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/book-covers-2x.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/book-covers-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/book-covers-2x.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/book-covers-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/book-covers-2x.png 2x"
                          />
                          <img
                            className={classes.img}
                            alt="Book Covers"
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/book-covers-2x.png"
                          />
                        </picture>
                      </a>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.subcategoryWrapper}>
                      <a
                        href="/categories/graphics-design/creative-logo-design?source=hplo_subcat_first_step&pos=1"
                        className={classes.aSubcategory}
                      >
                        <h4 className={classes.h4}>
                          <small className={classes.small}>
                            Build your brand
                          </small>
                          Logo Design
                        </h4>
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/logo-design-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/logo-design-2x.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/logo-design-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/logo-design-2x.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/logo-design-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/logo-design-2x.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/logo-design-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/logo-design-2x.png 2x"
                          />
                          <img
                            className={classes.img}
                            alt="Logo Design"
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/logo-design-2x.png"
                          />
                        </picture>
                      </a>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.subcategoryWrapper}>
                      <a
                        href="/categories/programming-tech/wordpress-services?source=hplo_subcat_first_step&pos=2"
                        className={classes.aSubcategory}
                      >
                        <h4 className={classes.h4}>
                          <small className={classes.small}>
                            Customize your site
                          </small>
                          WordPress
                        </h4>
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_1.0/v1/attachments/generic_asset/asset/ae11e2d45410b0eded7fba0e46b09dbd-1598561917003/wordpress-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_2.0/v1/attachments/generic_asset/asset/ae11e2d45410b0eded7fba0e46b09dbd-1598561917003/wordpress-2x.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_1.0/v1/attachments/generic_asset/asset/ae11e2d45410b0eded7fba0e46b09dbd-1598561917003/wordpress-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_2.0/v1/attachments/generic_asset/asset/ae11e2d45410b0eded7fba0e46b09dbd-1598561917003/wordpress-2x.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_1.0/v1/attachments/generic_asset/asset/ae11e2d45410b0eded7fba0e46b09dbd-1598561917003/wordpress-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_2.0/v1/attachments/generic_asset/asset/ae11e2d45410b0eded7fba0e46b09dbd-1598561917003/wordpress-2x.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/ae11e2d45410b0eded7fba0e46b09dbd-1598561917003/wordpress-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/ae11e2d45410b0eded7fba0e46b09dbd-1598561917003/wordpress-2x.png 2x"
                          />
                          <img
                            className={classes.img}
                            alt="WordPress"
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/ae11e2d45410b0eded7fba0e46b09dbd-1598561917003/wordpress-2x.png"
                          />
                        </picture>
                      </a>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.subcategoryWrapper}>
                      <a
                        href="/categories/music-audio/voice-overs?source=hplo_subcat_first_step&pos=3"
                        className={classes.aSubcategory}
                      >
                        <h4 className={classes.h4}>
                          <small className={classes.small}>
                            Share your message
                          </small>
                          Voice Over
                        </h4>
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741669/voiceover-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741669/voiceover-2x.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741669/voiceover-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741669/voiceover-2x.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741669/voiceover-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741669/voiceover-2x.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741669/voiceover-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741669/voiceover-2x.png 2x"
                          />
                          <img
                            className={classes.img}
                            alt="Voice Over"
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741669/voiceover-2x.png"
                          />
                        </picture>
                      </a>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.subcategoryWrapper}>
                      <a
                        href="/categories/video-animation/animated-explainer-videos?source=hplo_subcat_first_step&pos=4"
                        className={classes.aSubcategory}
                      >
                        <h4 className={classes.h4}>
                          <small className={classes.small}>
                            Engage your audience
                          </small>
                          Video Explainer
                        </h4>
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741663/animated-explainer-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741663/animated-explainer-2x.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741663/animated-explainer-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741663/animated-explainer-2x.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741663/animated-explainer-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741663/animated-explainer-2x.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741663/animated-explainer-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741663/animated-explainer-2x.png 2x"
                          />
                          <img
                            className={classes.img}
                            alt="Video Explainer"
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741663/animated-explainer-2x.png"
                          />
                        </picture>
                      </a>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.subcategoryWrapper}>
                      <a
                        href="/categories/online-marketing/social-marketing?source=hplo_subcat_first_step&pos=5"
                        className={classes.aSubcategory}
                      >
                        <h4 className={classes.h4}>
                          <small className={classes.small}>
                            Reach more customers
                          </small>
                          Social Media
                        </h4>
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741667/social-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741667/social-2x.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741667/social-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741667/social-2x.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741667/social-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741667/social-2x.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741667/social-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741667/social-2x.png 2x"
                          />
                          <img
                            className={classes.img}
                            alt="Social Media"
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741667/social-2x.png"
                          />
                        </picture>
                      </a>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.subcategoryWrapper}>
                      <a
                        href="/categories/online-marketing/seo-services?source=hplo_subcat_first_step&pos=6"
                        className={classes.aSubcategory}
                      >
                        <h4 className={classes.h4}>
                          <small className={classes.small}>
                            Unlock growth online
                          </small>
                          SEO
                        </h4>
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741668/seo-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741668/seo-2x.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741668/seo-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741668/seo-2x.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741668/seo-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741668/seo-2x.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741668/seo-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741668/seo-2x.png 2x"
                          />
                          <img
                            className={classes.img}
                            alt="SEO"
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741668/seo-2x.png"
                          />
                        </picture>
                      </a>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.subcategoryWrapper}>
                      <a
                        href="/categories/graphics-design/digital-illustration?source=hplo_subcat_first_step&pos=7"
                        className={classes.aSubcategory}
                      >
                        <h4 className={classes.h4}>
                          <small className={classes.small}>
                            Color your dreams
                          </small>
                          Illustration
                        </h4>
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/illustration-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/illustration-2x.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/illustration-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/illustration-2x.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/illustration-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/illustration-2x.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/illustration-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/illustration-2x.png 2x"
                          />
                          <img
                            className={classes.img}
                            alt="Illustration"
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/illustration-2x.png"
                          />
                        </picture>
                      </a>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.subcategoryWrapper}>
                      <a
                        href="/categories/writing-translation/quality-translation-services?source=hplo_subcat_first_step&pos=8"
                        className={classes.aSubcategory}
                      >
                        <h4 className={classes.h4}>
                          <small className={classes.small}>Go global</small>
                          Translation
                        </h4>
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741674/translation-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741674/translation-2x.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741674/translation-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741674/translation-2x.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741674/translation-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741674/translation-2x.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741674/translation-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741674/translation-2x.png 2x"
                          />
                          <img
                            className={classes.img}
                            alt="Translation"
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741674/translation-2x.png"
                          />
                        </picture>
                      </a>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.subcategoryWrapper}>
                      <a
                        href="/categories/data/data-entry?source=hplo_subcat_first_step&pos=9"
                        className={classes.aSubcategory}
                      >
                        <h4 className={classes.h4}>
                          <small className={classes.small}>
                            Learn your business
                          </small>
                          Data Entry
                        </h4>
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/data-entry-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/data-entry-2x.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/data-entry-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/data-entry-2x.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/data-entry-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/data-entry-2x.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/data-entry-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/data-entry-2x.png 2x"
                          />
                          <img
                            className={classes.img}
                            alt="Data Entry"
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/data-entry-2x.png"
                          />
                        </picture>
                      </a>
                    </div>
                  </div>
                </div>

                <div className={classes.slickSlide}>
                  <div>
                    <div className={classes.subcategoryWrapper}>
                      <a
                        href="/categories/graphics-design/book-design/cover?source=hplo_subcat_first_step&pos=10"
                        className={classes.aSubcategory}
                      >
                        <h4 className={classes.h4}>
                          <small className={classes.small}>
                            Showcase your story
                          </small>
                          Book Covers
                        </h4>
                        <picture>
                          <source
                            media="(min-width: 1060px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/book-covers-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_255,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/book-covers-2x.png 2x"
                          />
                          <source
                            media="(min-width: 800px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/book-covers-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_305,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/book-covers-2x.png 2x"
                          />
                          <source
                            media="(min-width: 600px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/book-covers-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/book-covers-2x.png 2x"
                          />
                          <source
                            media="(max-width: 599px)"
                            srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/book-covers-2x.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_2.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/book-covers-2x.png 2x"
                          />
                          <img
                            className={classes.img}
                            alt="Book Covers"
                            src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/book-covers-2x.png"
                          />
                        </picture>
                      </a>
                    </div>
                  </div>
                </div>
              </div>

              <button
                className={classes.buttonNext}
                id="Next-btn"
                onClick={clickNextOrPrev}
              ></button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
