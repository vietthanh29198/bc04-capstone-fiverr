import React from "react";
import { makeStyles } from "@mui/styles";

const useStyles = makeStyles({
  slickList: {
    display: "block",
    position: "relative",
    overflow: "hidden",
    margin: "0",
    padding: "0",
  },
  slickTrack: {
    display: "flex",
    justifyContent: "flex-start",
    position: "relative",
    left: "0px",
    transition: "left 500ms ease 0s",
    width: "9846px",
    opacity: "1",
    minWidth: "100%",
  },
  testimonial: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  testimonialModal: {
    width: "40%",
    minHeight: "250px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    position: "relative",
  },
  pictureWrapper: {
    position: "relative",
    cursor: "pointer",
    "&::after": {
      content: '""',
      display: "inline-block",
      backgroundImage: `url("https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/desktop-play-button.c1196d6.png")`,
      backgroundRepeat: "no-repeat",
      backgroundSize: "80px",
      width: "80px",
      height: "80px",
      position: "absolute",
      zIndex: "0",
      top: "50%",
      left: "50%",
      transform: "translate(-50%,-50%)",
    },
  },
  imgPictureWrapper: {
    width: "100%",
    height: "100%",
    borderRadius: "4px",
  },
  textContent: {
    padding: "0 48px 0 46px",
    maxWidth: "707px",
    width: "60%",
  },
  testimonialLogo: {
    borderLeft: "1px solid #c5c6c9",
    height: "36px",
    marginLeft: "16px",
  },
  imgTextContent: {
    height: "36px",
    display: "inline-block",
    verticalAlign: "middle",
    marginLeft: "12px",
    color: "#74767e",
  },
  fontDomaine: { fontSize: "30px", lineHeight: "44px", color: "#003912" },
  buttonPrev: {
    top: "50%",
    left: "-23px",
    cursor: "pointer",
    fontSize: "16px",
    lineHeight: "100%",
    textAlign: "center",
    position: "absolute",
    zIndex: "4",
    width: "48px",
    height: "48px",
    borderRadius: "30px",
    border: "0",
    boxShadow: "0 2px 5px 0 rgb(0 0 0 / 15%)",
    backgroundColor: "white",
    "&::after": {
      content: `''`,
      display: "inline-block",
      backgroundImage: `url("https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/arrows.a8b0bad.svg")`,
      backgroundRepeat: "no-repeat",
      backgroundSize: "16px 64px",
      width: "16px",
      height: "16px",
      marginTop: "3px",
    },
  },
  buttonNext: {
    top: "50%",
    right: "-23px",
    cursor: "pointer",
    fontSize: "16px",
    lineHeight: "100%",
    textAlign: "center",
    position: "absolute",
    zIndex: "4",
    width: "48px",
    height: "48px",
    borderRadius: "30px",
    border: "0",
    boxShadow: "0 2px 5px 0 rgb(0 0 0 / 15%)",
    backgroundColor: "white",
    "&::after": {
      content: `''`,
      display: "inline-block",
      backgroundImage: `url("https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/arrows.a8b0bad.svg")`,
      backgroundRepeat: "no-repeat",
      backgroundSize: "16px 64px",
      width: "16px",
      height: "16px",
      marginTop: "3px",
      backgroundPosition: "0 -48px",
    },
  },
  h5: {
    fontWeight: "400",
    color: "#74767e",
    paddingBottom: "16px",
    margin: "0",
    fontSize: "20px",
    lineHeight: "140%",
  },
});

export default function Testimonials() {
  const classes = useStyles();

  function clickNextOrPrev() {
    var listItems = document.getElementById("slick-track-testimonial");

    listItems.style.left =
      listItems.style.left == "0px" || listItems.style.left == "-1440px"
        ? listItems.style.left.substring(0, listItems.style.left.length - 2) -
          1440 +
          "px"
        : "0px";
  }

  return (
    <div
      className="testimonials-container"
      style={{
        margin: "0 0 96px",
        padding: "0 32px",
      }}
    >
      <div className="slider-package">
        <div
          className="slick-slider"
          style={{ margin: "0", position: "relative", boxSizing: "border-box" }}
        >
          <div>
            <button
              className={classes.buttonPrev}
              id="Previous-btn"
              onClick={clickNextOrPrev}
            ></button>

            <div className={classes.slickList}>
              <div className={classes.slickTrack} id="slick-track-testimonial">
                <div className="slick-slide">
                  <div>
                    <div className={classes.testimonial}>
                      <div className={classes.testimonialModal}>
                        <div className={classes.pictureWrapper}>
                          <picture>
                            <source
                              media="(min-width: 1160px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_560,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173414/testimonial-video-still-naadam.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_560,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173414/testimonial-video-still-naadam.jpg 2x"
                            />
                            <source
                              media="(min-width: 900px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_450,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173414/testimonial-video-still-naadam.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_450,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173414/testimonial-video-still-naadam.jpg 2x"
                            />
                            <source
                              media="(min-width: 600px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_820,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173414/testimonial-video-still-naadam.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_820,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173414/testimonial-video-still-naadam.jpg 2x"
                            />
                            <source
                              media="(min-width: 361px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_480,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173414/testimonial-video-still-naadam.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_480,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173414/testimonial-video-still-naadam.jpg 2x"
                            />
                            <source
                              media="(max-width: 360px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173414/testimonial-video-still-naadam.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173414/testimonial-video-still-naadam.jpg 2x"
                            />
                            <img
                              className={classes.imgPictureWrapper}
                              alt="Video teaser image"
                              src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_560,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173414/testimonial-video-still-naadam.jpg"
                              loading="lazy"
                            />
                          </picture>
                        </div>
                      </div>

                      <div className={classes.textContent}>
                        <h5 className={classes.h5}>
                          Caitlin Tormey, Chief Commercial Officer
                          <span className={classes.testimonialLogo}>
                            <img
                              className={classes.imgTextContent}
                              alt="Company logo"
                              src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/naadam-logo-x2.0a3b198.png"
                              loading="lazy"
                            />
                          </span>
                        </h5>
                        <blockquote className={classes.fontDomaine}>
                          <i style={{ fontWeight: "500" }}>
                            "We've used Fiverr for Shopify web development,
                            graphic design, and backend web development. Working
                            with Fiverr makes my job a little easier every day."
                          </i>
                        </blockquote>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="slick-slide">
                  <div>
                    <div className={classes.testimonial}>
                      <div className={classes.testimonialModal}>
                        <div className={classes.pictureWrapper}>
                          <picture>
                            <source
                              media="(min-width: 1160px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_560,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173399/testimonial-video-still-rooted.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_560,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173399/testimonial-video-still-rooted.jpg 2x"
                            />
                            <source
                              media="(min-width: 900px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_450,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173399/testimonial-video-still-rooted.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_450,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173399/testimonial-video-still-rooted.jpg 2x"
                            />
                            <source
                              media="(min-width: 600px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_820,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173399/testimonial-video-still-rooted.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_820,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173399/testimonial-video-still-rooted.jpg 2x"
                            />
                            <source
                              media="(min-width: 361px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_480,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173399/testimonial-video-still-rooted.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_480,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173399/testimonial-video-still-rooted.jpg 2x"
                            />
                            <source
                              media="(max-width: 360px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173399/testimonial-video-still-rooted.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173399/testimonial-video-still-rooted.jpg 2x"
                            />
                            <img
                              className={classes.imgPictureWrapper}
                              alt="Video teaser image"
                              src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_560,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173399/testimonial-video-still-rooted.jpg"
                              loading="lazy"
                            />
                          </picture>
                        </div>
                      </div>
                      <div className={classes.textContent}>
                        <h5 className={classes.h5}>
                          Kay Kim, Co-Founder
                          <span className={classes.testimonialLogo}>
                            <img
                              className={classes.imgTextContent}
                              alt="Company logo"
                              src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/rooted-logo-x2.321d79d.png"
                              loading="lazy"
                            />
                          </span>
                        </h5>
                        <blockquote className={classes.fontDomaine}>
                          <i style={{ fontWeight: "500" }}>
                            "It's extremely exciting that Fiverr has freelancers
                            from all over the world — it broadens the talent
                            pool. One of the best things about Fiverr is that
                            while we're sleeping, someone's working."
                          </i>
                        </blockquote>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="slick-slide">
                  <div>
                    <div className={classes.testimonial}>
                      <div className={classes.testimonialModal}>
                        <div className={classes.pictureWrapper}>
                          <picture>
                            <source
                              media="(min-width: 1160px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_560,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173396/testimonial-video-still-lavender.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_560,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173396/testimonial-video-still-lavender.jpg 2x"
                            />
                            <source
                              media="(min-width: 900px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_450,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173396/testimonial-video-still-lavender.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_450,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173396/testimonial-video-still-lavender.jpg 2x"
                            />
                            <source
                              media="(min-width: 600px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_820,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173396/testimonial-video-still-lavender.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_820,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173396/testimonial-video-still-lavender.jpg 2x"
                            />
                            <source
                              media="(min-width: 361px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_480,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173396/testimonial-video-still-lavender.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_480,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173396/testimonial-video-still-lavender.jpg 2x"
                            />
                            <source
                              media="(max-width: 360px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173396/testimonial-video-still-lavender.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173396/testimonial-video-still-lavender.jpg 2x"
                            />
                            <img
                              className={classes.imgPictureWrapper}
                              alt="Video teaser image"
                              src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_560,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173396/testimonial-video-still-lavender.jpg"
                              loading="lazy"
                            />
                          </picture>
                        </div>
                      </div>
                      <div className={classes.textContent}>
                        <h5 className={classes.h5}>
                          Brighid Gannon (DNP, PMHNP-BC), Co-Founder
                          <span className={classes.testimonialLogo}>
                            <img
                              className={classes.imgTextContent}
                              alt="Company logo"
                              src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/lavender-logo-x2.89c5e2e.png"
                              loading="lazy"
                            />
                          </span>
                        </h5>
                        <blockquote className={classes.fontDomaine}>
                          <i style={{ fontWeight: "500" }}>
                            "We used Fiverr for SEO, our logo, website, copy,
                            animated videos — literally everything. It was like
                            working with a human right next to you versus being
                            across the world."
                          </i>
                        </blockquote>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="slick-slide">
                  <div>
                    <div className={classes.testimonial}>
                      <div className={classes.testimonialModal}>
                        <div className={classes.pictureWrapper}>
                          <picture>
                            <source
                              media="(min-width: 1160px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_560,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173395/testimonial-video-still-haerfest.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_560,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173395/testimonial-video-still-haerfest.jpg 2x"
                            />
                            <source
                              media="(min-width: 900px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_450,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173395/testimonial-video-still-haerfest.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_450,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173395/testimonial-video-still-haerfest.jpg 2x"
                            />
                            <source
                              media="(min-width: 600px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_820,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173395/testimonial-video-still-haerfest.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_820,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173395/testimonial-video-still-haerfest.jpg 2x"
                            />
                            <source
                              media="(min-width: 361px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_480,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173395/testimonial-video-still-haerfest.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_480,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173395/testimonial-video-still-haerfest.jpg 2x"
                            />
                            <source
                              media="(max-width: 360px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173395/testimonial-video-still-haerfest.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173395/testimonial-video-still-haerfest.jpg 2x"
                            />
                            <img
                              className={classes.imgPictureWrapper}
                              alt="Video teaser image"
                              src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_560,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173395/testimonial-video-still-haerfest.jpg"
                              loading="lazy"
                            />
                          </picture>
                        </div>
                      </div>
                      <div className={classes.textContent}>
                        <h5 className={classes.h5}>
                          Tim and Dan Joo, Co-Founders
                          <span className={classes.testimonialLogo}>
                            <img
                              className={classes.imgTextContent}
                              alt="Company logo"
                              src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/haerfest-logo-x2.03fa5c5.png"
                              loading="lazy"
                            />
                          </span>
                        </h5>
                        <blockquote className={classes.fontDomaine}>
                          <i style={{ fontWeight: "500" }}>
                            "When you want to create a business bigger than
                            yourself, you need a lot of help. That's what Fiverr
                            does."
                          </i>
                        </blockquote>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="slick-slide">
                  <div>
                    <div className={classes.testimonial}>
                      <div className={classes.testimonialModal}>
                        <div className={classes.pictureWrapper}>
                          <picture>
                            <source
                              media="(min-width: 1160px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_560,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173414/testimonial-video-still-naadam.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_560,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173414/testimonial-video-still-naadam.jpg 2x"
                            />
                            <source
                              media="(min-width: 900px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_450,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173414/testimonial-video-still-naadam.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_450,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173414/testimonial-video-still-naadam.jpg 2x"
                            />
                            <source
                              media="(min-width: 600px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_820,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173414/testimonial-video-still-naadam.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_820,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173414/testimonial-video-still-naadam.jpg 2x"
                            />
                            <source
                              media="(min-width: 361px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_480,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173414/testimonial-video-still-naadam.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_480,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173414/testimonial-video-still-naadam.jpg 2x"
                            />
                            <source
                              media="(max-width: 360px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173414/testimonial-video-still-naadam.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173414/testimonial-video-still-naadam.jpg 2x"
                            />
                            <img
                              className={classes.imgPictureWrapper}
                              alt="Video teaser image"
                              src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_560,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173414/testimonial-video-still-naadam.jpg"
                              loading="lazy"
                            />
                          </picture>
                        </div>
                      </div>
                      <div className={classes.textContent}>
                        <h5 className={classes.h5}>
                          Caitlin Tormey, Chief Commercial Officer
                          <span className={classes.testimonialLogo}>
                            <img
                              className={classes.imgTextContent}
                              alt="Company logo"
                              src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/naadam-logo-x2.0a3b198.png"
                              loading="lazy"
                            />
                          </span>
                        </h5>
                        <blockquote className={classes.fontDomaine}>
                          <i style={{ fontWeight: "500" }}>
                            "We've used Fiverr for Shopify web development,
                            graphic design, and backend web development. Working
                            with Fiverr makes my job a little easier every day."
                          </i>
                        </blockquote>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="slick-slide">
                  <div>
                    <div className={classes.testimonial}>
                      <div className={classes.testimonialModal}>
                        <div className={classes.pictureWrapper}>
                          <picture>
                            <source
                              media="(min-width: 1160px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_560,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173399/testimonial-video-still-rooted.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_560,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173399/testimonial-video-still-rooted.jpg 2x"
                            />
                            <source
                              media="(min-width: 900px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_450,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173399/testimonial-video-still-rooted.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_450,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173399/testimonial-video-still-rooted.jpg 2x"
                            />
                            <source
                              media="(min-width: 600px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_820,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173399/testimonial-video-still-rooted.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_820,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173399/testimonial-video-still-rooted.jpg 2x"
                            />
                            <source
                              media="(min-width: 361px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_480,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173399/testimonial-video-still-rooted.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_480,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173399/testimonial-video-still-rooted.jpg 2x"
                            />
                            <source
                              media="(max-width: 360px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173399/testimonial-video-still-rooted.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173399/testimonial-video-still-rooted.jpg 2x"
                            />
                            <img
                              className={classes.imgPictureWrapper}
                              alt="Video teaser image"
                              src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_560,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173399/testimonial-video-still-rooted.jpg"
                              loading="lazy"
                            />
                          </picture>
                        </div>
                      </div>
                      <div className={classes.textContent}>
                        <h5 className={classes.h5}>
                          Kay Kim, Co-Founder
                          <span className={classes.testimonialLogo}>
                            <img
                              className={classes.imgTextContent}
                              alt="Company logo"
                              src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/rooted-logo-x2.321d79d.png"
                              loading="lazy"
                            />
                          </span>
                        </h5>
                        <blockquote className={classes.fontDomaine}>
                          <i style={{ fontWeight: "500" }}>
                            "It's extremely exciting that Fiverr has freelancers
                            from all over the world — it broadens the talent
                            pool. One of the best things about Fiverr is that
                            while we're sleeping, someone's working."
                          </i>
                        </blockquote>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="slick-slide">
                  <div>
                    <div className={classes.testimonial}>
                      <div className={classes.testimonialModal}>
                        <div className={classes.pictureWrapper}>
                          <picture>
                            <source
                              media="(min-width: 1160px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_560,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173396/testimonial-video-still-lavender.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_560,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173396/testimonial-video-still-lavender.jpg 2x"
                            />
                            <source
                              media="(min-width: 900px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_450,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173396/testimonial-video-still-lavender.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_450,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173396/testimonial-video-still-lavender.jpg 2x"
                            />
                            <source
                              media="(min-width: 600px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_820,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173396/testimonial-video-still-lavender.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_820,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173396/testimonial-video-still-lavender.jpg 2x"
                            />
                            <source
                              media="(min-width: 361px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_480,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173396/testimonial-video-still-lavender.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_480,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173396/testimonial-video-still-lavender.jpg 2x"
                            />
                            <source
                              media="(max-width: 360px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173396/testimonial-video-still-lavender.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173396/testimonial-video-still-lavender.jpg 2x"
                            />
                            <img
                              className={classes.imgPictureWrapper}
                              alt="Video teaser image"
                              src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_560,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173396/testimonial-video-still-lavender.jpg"
                              loading="lazy"
                            />
                          </picture>
                        </div>
                      </div>
                      <div className={classes.textContent}>
                        <h5 className={classes.h5}>
                          Brighid Gannon (DNP, PMHNP-BC), Co-Founder
                          <span className={classes.testimonialLogo}>
                            <img
                              className={classes.imgTextContent}
                              alt="Company logo"
                              src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/lavender-logo-x2.89c5e2e.png"
                              loading="lazy"
                            />
                          </span>
                        </h5>
                        <blockquote className={classes.fontDomaine}>
                          <i style={{ fontWeight: "500" }}>
                            "We used Fiverr for SEO, our logo, website, copy,
                            animated videos — literally everything. It was like
                            working with a human right next to you versus being
                            across the world."
                          </i>
                        </blockquote>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="slick-slide">
                  <div>
                    <div className={classes.testimonial}>
                      <div className={classes.testimonialModal}>
                        <div className={classes.pictureWrapper}>
                          <picture>
                            <source
                              media="(min-width: 1160px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_560,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173395/testimonial-video-still-haerfest.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_560,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173395/testimonial-video-still-haerfest.jpg 2x"
                            />
                            <source
                              media="(min-width: 900px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_450,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173395/testimonial-video-still-haerfest.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_450,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173395/testimonial-video-still-haerfest.jpg 2x"
                            />
                            <source
                              media="(min-width: 600px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_820,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173395/testimonial-video-still-haerfest.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_820,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173395/testimonial-video-still-haerfest.jpg 2x"
                            />
                            <source
                              media="(min-width: 361px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_480,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173395/testimonial-video-still-haerfest.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_480,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173395/testimonial-video-still-haerfest.jpg 2x"
                            />
                            <source
                              media="(max-width: 360px)"
                              srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173395/testimonial-video-still-haerfest.jpg 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_2.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173395/testimonial-video-still-haerfest.jpg 2x"
                            />
                            <img
                              className={classes.imgPictureWrapper}
                              alt="Video teaser image"
                              src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_560,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173395/testimonial-video-still-haerfest.jpg"
                              loading="lazy"
                            />
                          </picture>
                        </div>
                      </div>
                      <div className={classes.textContent}>
                        <h5 className={classes.h5}>
                          Tim and Dan Joo, Co-Founders
                          <span className={classes.testimonialLogo}>
                            <img
                              className={classes.imgTextContent}
                              alt="Company logo"
                              src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/haerfest-logo-x2.03fa5c5.png"
                              loading="lazy"
                            />
                          </span>
                        </h5>
                        <blockquote className={classes.fontDomaine}>
                          <i style={{ fontWeight: "500" }}>
                            "When you want to create a business bigger than
                            yourself, you need a lot of help. That's what Fiverr
                            does."
                          </i>
                        </blockquote>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <button
              className={classes.buttonNext}
              id="Next-btn"
              onClick={clickNextOrPrev}
            ></button>
          </div>
        </div>
      </div>
    </div>
  );
}
