import React from "react";
import { makeStyles } from "@mui/styles";

const useStyles = makeStyles({
  sellingPropositionWrapper: {
    marginBottom: "96px",
    backgroundColor: "#f1fdf7",
    margin: "0",
  },
  sellingProposition: {
    padding: "96px 32px",
    margin: "0",
    maxWidth: "1400px",
    width: "100%",
  },
  flexSellingProposition: {
    margin: "0 -16px",
    display: "flex",
    flexGrow: "0",
    flexShrink: "1",
    flexBasis: "auto",
    position: "relative",
    alignItems: "center",
  },
  sellingText: {
    paddingRight: "11%",
    flexBasis: "46.66667%",
    maxWidth: "46.66667%",
  },
  sellingVideo: {
    paddingRight: "0 16px",
    flexBasis: "53.33333%",
    maxWidth: "53.33333%",
  },
  videoModal: {
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "center",
    position: "relative",
  },
  h2: {
    fontSize: "32px",
    lineHeight: "120%",
    padding: "0 0 24px",
    color: "#404145",
    margin: "0",
  },
  ul: { listStyle: "none", listStyleImage: "none", margin: "0", padding: "0" },
  li: {
    padding: "0 0 24px",
    margin: "0",
  },
  h6: {
    padding: "0 0 8px",
    display: "flex",
    alignItems: "flex-start",
    fontSize: "18px",
    lineHeight: "140%",
    margin: "0",
    color: "#404145",
  },
  span: {
    width: "24px",
    height: "24px",
    fill: " rgb(122, 125, 133)",
    marginRight: "10px",
    minWidth: "24px",
  },
  p: {
    fontSize: "18px",
    lineHeight: "26px",
    margin: "0",
    color: "#62646A",
  },
  pictureWrapper: {
    position: "relative",
    cursor: "pointer",
    "&::after": {
      content: '""',
      display: "inline-block",
      backgroundImage: `url("https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/desktop-play-button.c1196d6.png")`,
      backgroundRepeat: "no-repeat",
      backgroundSize: "80px",
      width: "80px",
      height: "80px",
      position: "absolute",
      zIndex: "0",
      top: "50%",
      left: "50%",
      transform: "translate(-50%,-50%)",
    },
  },
});

export default function Selling() {
  const classes = useStyles();

  return (
    <div className={classes.sellingPropositionWrapper}>
      <div className={classes.sellingProposition}>
        <div className={classes.flexSellingProposition}>
          <div className={classes.sellingText}>
            <h2 className={classes.h2}>
              A whole world of freelance talent at your fingertips
            </h2>

            <ul className={classes.ul}>
              <li className={classes.li}>
                <h6 className={classes.h6}>
                  <span className={classes.span} aria-hidden="true">
                    <svg
                      width={24}
                      height={24}
                      viewBox="0 0 16 16"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M8 1.75C4.54822 1.75 1.75 4.54822 1.75 8C1.75 11.4518 4.54822 14.25 8 14.25C11.4518 14.25 14.25 11.4518 14.25 8C14.25 4.54822 11.4518 1.75 8 1.75ZM0.25 8C0.25 3.71979 3.71979 0.25 8 0.25C12.2802 0.25 15.75 3.71979 15.75 8C15.75 12.2802 12.2802 15.75 8 15.75C3.71979 15.75 0.25 12.2802 0.25 8Z" />
                      <path d="M11.5303 5.46967C11.8232 5.76256 11.8232 6.23744 11.5303 6.53033L7.53033 10.5303C7.23744 10.8232 6.76256 10.8232 6.46967 10.5303L4.46967 8.53033C4.17678 8.23744 4.17678 7.76256 4.46967 7.46967C4.76256 7.17678 5.23744 7.17678 5.53033 7.46967L7 8.93934L10.4697 5.46967C10.7626 5.17678 11.2374 5.17678 11.5303 5.46967Z" />
                    </svg>
                  </span>
                  The best for every budget
                </h6>
                <p className={classes.p}>
                  Find high-quality services at every price point. No hourly
                  rates, just project-based pricing.
                </p>
              </li>

              <li className={classes.li}>
                <h6 className={classes.h6}>
                  <span className={classes.span} aria-hidden="true">
                    <svg
                      width={24}
                      height={24}
                      viewBox="0 0 16 16"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M8 1.75C4.54822 1.75 1.75 4.54822 1.75 8C1.75 11.4518 4.54822 14.25 8 14.25C11.4518 14.25 14.25 11.4518 14.25 8C14.25 4.54822 11.4518 1.75 8 1.75ZM0.25 8C0.25 3.71979 3.71979 0.25 8 0.25C12.2802 0.25 15.75 3.71979 15.75 8C15.75 12.2802 12.2802 15.75 8 15.75C3.71979 15.75 0.25 12.2802 0.25 8Z" />
                      <path d="M11.5303 5.46967C11.8232 5.76256 11.8232 6.23744 11.5303 6.53033L7.53033 10.5303C7.23744 10.8232 6.76256 10.8232 6.46967 10.5303L4.46967 8.53033C4.17678 8.23744 4.17678 7.76256 4.46967 7.46967C4.76256 7.17678 5.23744 7.17678 5.53033 7.46967L7 8.93934L10.4697 5.46967C10.7626 5.17678 11.2374 5.17678 11.5303 5.46967Z" />
                    </svg>
                  </span>
                  Quality work done quickly
                </h6>
                <p className={classes.p}>
                  Find the right freelancer to begin working on your project
                  within minutes.
                </p>
              </li>

              <li className={classes.li}>
                <h6 className={classes.h6}>
                  <span className={classes.span} aria-hidden="true">
                    <svg
                      width={24}
                      height={24}
                      viewBox="0 0 16 16"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M8 1.75C4.54822 1.75 1.75 4.54822 1.75 8C1.75 11.4518 4.54822 14.25 8 14.25C11.4518 14.25 14.25 11.4518 14.25 8C14.25 4.54822 11.4518 1.75 8 1.75ZM0.25 8C0.25 3.71979 3.71979 0.25 8 0.25C12.2802 0.25 15.75 3.71979 15.75 8C15.75 12.2802 12.2802 15.75 8 15.75C3.71979 15.75 0.25 12.2802 0.25 8Z" />
                      <path d="M11.5303 5.46967C11.8232 5.76256 11.8232 6.23744 11.5303 6.53033L7.53033 10.5303C7.23744 10.8232 6.76256 10.8232 6.46967 10.5303L4.46967 8.53033C4.17678 8.23744 4.17678 7.76256 4.46967 7.46967C4.76256 7.17678 5.23744 7.17678 5.53033 7.46967L7 8.93934L10.4697 5.46967C10.7626 5.17678 11.2374 5.17678 11.5303 5.46967Z" />
                    </svg>
                  </span>
                  Protected payments, every time
                </h6>
                <p className={classes.p}>
                  Always know what you'll pay upfront. Your payment isn't
                  released until you approve the work.
                </p>
              </li>

              <li className={classes.li}>
                <h6 className={classes.h6}>
                  <span className={classes.span} aria-hidden="true">
                    <svg
                      width={24}
                      height={24}
                      viewBox="0 0 16 16"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M8 1.75C4.54822 1.75 1.75 4.54822 1.75 8C1.75 11.4518 4.54822 14.25 8 14.25C11.4518 14.25 14.25 11.4518 14.25 8C14.25 4.54822 11.4518 1.75 8 1.75ZM0.25 8C0.25 3.71979 3.71979 0.25 8 0.25C12.2802 0.25 15.75 3.71979 15.75 8C15.75 12.2802 12.2802 15.75 8 15.75C3.71979 15.75 0.25 12.2802 0.25 8Z" />
                      <path d="M11.5303 5.46967C11.8232 5.76256 11.8232 6.23744 11.5303 6.53033L7.53033 10.5303C7.23744 10.8232 6.76256 10.8232 6.46967 10.5303L4.46967 8.53033C4.17678 8.23744 4.17678 7.76256 4.46967 7.46967C4.76256 7.17678 5.23744 7.17678 5.53033 7.46967L7 8.93934L10.4697 5.46967C10.7626 5.17678 11.2374 5.17678 11.5303 5.46967Z" />
                    </svg>
                  </span>
                  24/7 support
                </h6>
                <p className={classes.p}>
                  Questions? Our round-the-clock support team is available to
                  help anytime, anywhere.
                </p>
              </li>
            </ul>
          </div>

          <div className={classes.sellingVideo}>
            <div className={classes.videoModal}>
              <div className={classes.pictureWrapper}>
                <picture style={{ display: "flex" }}>
                  <source
                    media="(min-width: 1160px)"
                    srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_700,dpr_1.0/v1/attachments/generic_asset/asset/089e3bb9352f90802ad07ad9f6a4a450-1599517407052/selling-proposition-still-1400-x1.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_700,dpr_2.0/v1/attachments/generic_asset/asset/089e3bb9352f90802ad07ad9f6a4a450-1599517407052/selling-proposition-still-1400-x1.png 2x"
                  />
                  <source
                    media="(min-width: 900px)"
                    srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_600,dpr_1.0/v1/attachments/generic_asset/asset/089e3bb9352f90802ad07ad9f6a4a450-1599517407052/selling-proposition-still-1400-x1.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_600,dpr_2.0/v1/attachments/generic_asset/asset/089e3bb9352f90802ad07ad9f6a4a450-1599517407052/selling-proposition-still-1400-x1.png 2x"
                  />
                  <source
                    media="(min-width: 600px)"
                    srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_900,dpr_1.0/v1/attachments/generic_asset/asset/089e3bb9352f90802ad07ad9f6a4a450-1599517407052/selling-proposition-still-1400-x1.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_900,dpr_2.0/v1/attachments/generic_asset/asset/089e3bb9352f90802ad07ad9f6a4a450-1599517407052/selling-proposition-still-1400-x1.png 2x"
                  />
                  <source
                    media="(min-width: 361px)"
                    srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_600,dpr_1.0/v1/attachments/generic_asset/asset/089e3bb9352f90802ad07ad9f6a4a450-1599517407052/selling-proposition-still-1400-x1.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_600,dpr_2.0/v1/attachments/generic_asset/asset/089e3bb9352f90802ad07ad9f6a4a450-1599517407052/selling-proposition-still-1400-x1.png 2x"
                  />
                  <source
                    media="(max-width: 360px)"
                    srcSet="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_1.0/v1/attachments/generic_asset/asset/089e3bb9352f90802ad07ad9f6a4a450-1599517407052/selling-proposition-still-1400-x1.png 1x, https://fiverr-res.cloudinary.com/q_auto,f_auto,w_360,dpr_2.0/v1/attachments/generic_asset/asset/089e3bb9352f90802ad07ad9f6a4a450-1599517407052/selling-proposition-still-1400-x1.png 2x"
                  />
                  <img
                    style={{ width: "100%" }}
                    alt="Video teaser image"
                    src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_700,dpr_1.0/v1/attachments/generic_asset/asset/089e3bb9352f90802ad07ad9f6a4a450-1599517407052/selling-proposition-still-1400-x1.png"
                    loading="auto"
                  />
                </picture>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
